---
title: Colophon
date: 2020-06-07T18:08:54+02:00
weight: 4
---

### Colophon

Polices de caractère : Neue Haas Grotesk + Times New Roman.
Mise en page avec Paged.js par Timothée Goguely.

Image de couverture : Charles Gleyre, <em>Les Illusions perdues</em>, 1843 (crédit photo : Françoise Foliot).

Édité par design↔commun en novembre 2020.
[designcommun.fr](https://designcommun.fr/)

Cette œuvre est mise à disposition selon les termes de la Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International (CC BY-NC-SA 4.0).
