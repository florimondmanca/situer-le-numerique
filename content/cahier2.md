---
title: 'Cahier n°2 – Matérialiser le numérique : quelles infrastructures et quels impacts ?'
date: 2020-06-07T18:08:54.000+02:00
weight: 3
---

## Matérialiser le numérique : quelles infrastructures et quels impacts ?

### Comprendre l’infrastructure du numérique

#### Deux mythes
Il est important de définir le cadre dans lequel évolue le numérique car cela renverse la façon dont nous examinons traditionnellement son infrastructure et ses services. Les acteurs du secteur cherchent à montrer quel monde idéal est possible grâce au numérique. Les campagnes de promotion, les rapports, les scénarios produits aujourd’hui vont généralement dans ce sens. Des titres promotionnels comme “le futur de la mobilité grâce à l’IA”, “le monde sans limites de la 5G” ou incluant les mots “intelligent”, “smart”, “futur”, “connecté”, sont des exemples parmi d’autres de ce phénomène. L’avantage d’une telle position est qu’elle permet de décrire un monde simplifié depuis des technologies numériques en s’affranchissant des contraintes matérielles et écologiques. En définissant le cadre terrestre, ainsi que des objectifs et contraintes à court, moyen et long-terme, nous pouvons alors retourner la proposition initiale. Plutôt que de savoir quel monde est possible grâce au numérique, cherchons plutôt à comprendre quel(s) numérique(s) est(sont) possible(s) sur la Terre que nous voulons de définir.

> Passer de “quel monde est possible grâce au numérique” à “quel numérique est possible dans ce monde”.

Le numérique, tel que nous le définissons aujourd’hui, s’est développé dans la société civile à partir des années 90. Lorsque le numérique est proposé comme une offre commerciale grand public deux discours l’accompagnent : la dématérialisation et le “village global”. Premièrement, il semble a priori qu’aucune autre infrastructure globale n’ait été présentée comme “dématérialisée” avant. De ce point de vue, l’infrastructure numérique est un cas unique dont il faut prendre toute la mesure. Toute infrastructure est matérielle, par contre elle peut être camouflée par différents choix politiques, techniques, sociaux et économiques, et par différents choix de conception. Ce camouflage revient à “dématérialiser” un service courant (courrier) et à la “rematérialiser” loin de l’usager (email). L’envoi d’un courrier par la poste demande d’écrire sur une feuille de papier, d’avoir une enveloppe et un timbre et d’aller à la boîte aux lettres. Le courrier sera récupéré par un facteur en vélo ou en fourgon, amené à un centre de tri et livré à destination par un autre facteur. Cette opération nécessite de l’engagement personnel (écrire, achat papier/enveloppe/timbre, déplacement), des moyens de transport (fourgon de la poste), des lieux (boite aux lettres, centre de tri), de l’énergie (essence du fourgon, électricité du centre et chauffage). L’envoi d’un email nécessite une box, du cuivre, de la fibre dans les rues, des tableaux d’accès, des centres de données, des antennes, des ordinateurs et sûrement des smartphones. Cette opération nécessite peu d’engagement personnel (écrire), des moyens de transport (camion pour maintenance et travaux), de l’énergie (électricité des équipements utilisateurs, centres de données, climatisation, etc.).

{{< aside >}}
#### Internet et/ou Web ?
“Internet” est une infrastructure permettant l’interconnexion entre réseaux (un réseau de réseaux). Le “Web” (World Wide Web ou “www”) est un service qui utilise cette infrastructure pour accéder à des informations. Le web ne regroupe donc pas tous les informations disponibles sur l’Internet. Si Internet est une bibliothèque, alors le web est seulement une aile du bâtiment.

![Encadré 2](media/encadre2.png "Schéma")

{{< /aside >}}

Le “numérique” est donc bien une infrastructure matérielle sauf que son développement a plutôt eu tendance à la rendre invisible. Peu d’entre nous ont vu ou visité un centre de données ou savent repérer une antenne 4G. Dans notre quotidien, les seules choses qui nous lient à cette infrastructure sont principalement nos appareils personnels ou professionnels (ordinateur, smartphone, objets connectés) et nos box internet.

Ce processus de camouflage a été aussi lié au concept de “village global” qui sous-entendait que l’instantanéité des communications mondiales allait mener à l’émergence d’une culture globale. Puisqu’une partie croissante de l’humanité allait pouvoir échanger des informations, certains penseurs, chefs d’entreprise et investisseurs supposaient qu’une culture mondiale allait se développer. Ce principe présumait que les cultures pouvaient se détacher de leurs territoires et se mélanger dans un espace sans distance nommé “village global”. Cela repose sur une idée de la culture vue comme un concept immatériel, pourtant une culture repose sur des éléments bien matériels et des territoires spécifiques : les cuisines, les paysages, les langues, les corps, les musiques, les animaux, les plantes, l’architecture, etc. De plus, si nous imaginons que nous partageons tous le même espace cela détourne notre attention sur l’espace physique et géographique où est déployé l’infrastructure et les impacts de celle-ci sur des espaces réels (évacuation de chaleur des centres de données, utilisation d’eau, exploitation minière, etc). Les usages “majeurs” (*mainstream*) de cette infrastructure ont été largement pensé dans une logique universelle où le numérique est un drap de services et de solutions qui vient se déposer uniformément sur tous les territoires, quelque soit leurs spécificités et leurs besoins. Ainsi peu de services numériques majeurs ont été pensés depuis les besoins des zones rurales et périphériques. Ces territoires sont généralement les derniers à être recouvert par le “drap” du numérique. Finalement, un “village global” ne définit aucun territoire géographique, il est donc possible en son sein de faire vivre le concept de la “dématérialisation” et de l’étendre à d’autres termes nébuleux comme le cloud (serveur distant). Ces deux concepts sont aujourd’hui des mythes plus que des descriptions pertinentes de cette infrastructure.

{{< aside >}}
#### Le village global
Le “village global” est une métaphore proposée par Marshall Mcluhan pour représenter notre planète comme un seul et vaste village, où les nouvelles technologies et la mondialisation ont créé une interdépendance généralisée. Cette expression est tirée du livre “The Medium is the Message” publié en 1967. Le concept sera repris par plusieurs économistes et spécialistes du marketing pour désigner un marché global, allant parfois à l’encontre du terme de Mcluhan. Pour ce dernier, le village global n’est pas un espace d’échanges uniforme mais un lieu de discontinuités, de diversité et de division.
{{< /aside >}}

#### Décrire une infrastructure
L’histoire du numérique est avant tout l’histoire de la mise en place et du développement d’une infrastructure. Sa mise en place nécessite la coopération de forces politiques, industrielles, financières, scientifiques et sociales. En effet, une infrastructure n’est jamais “que” technique, elle est aussi sociale, c’est-à-dire qu’elle mobilise *a minima* des acteurs sociaux et politiques.

L’infrastructure numérique est enchevêtrée dans de nombreux phénomènes. Dans un premier temps, des “ressources” sont nécessaires, nous avons donc besoin de transformer de la matière en marchandise, c’est-à-dire une matière soumise au régime de propriété privée et qui est liée à un coût de production et à un prix sur un marché. De quelles ressources avons-nous besoin ? Une cinquantaine de métaux dont certains en très grande quantité; de l’énergie pour l’extraction, le transport, la transformation; et différentes matières premières, par exemple, du caoutchouc pour l’enrobage des câbles. Des mines et des plantations doivent donc être déployées ou sollicitées. Cela implique des savoirs scientifiques d’exploration d’un territoire pour quantifier les réserves souterraines des sols, une politique de privatisation des terres négociée avec des gouvernements locaux ou nationaux, l’intégration d’un droit du travail spécifique aux mineurs, des législations et des normes sanitaires et environnementales plus ou moins strictes en fonction des pays, le recrutement de la main d’oeuvre et donc des recruteurs, la mise en place de chaînes d’approvisionnement du matériel et la préparation du terrain à exploiter. Au final, comme presque toutes les infrastructures modernes, le numérique commence dès l’exploration minière des sols et leur préparation.

![Carottes de minerai à côté d’un ordinateur portable de géologue, départ et arrivée de l’infrastructure numérique](media/carottes.jpg "Shutterstock")

Dans un second temps ces ressources nécessitent d’être transportées et transformées. Le transport s’opère par camions et par bateaux. Ce processus s’inscrit donc dans des systèmes logistiques qui incluent droits maritimes, ports, travailleurs, douanes, syndicats, approvisionnement énergétique (fuel et kérosène pour les cargos). Les ressources acheminées doivent ensuite être transformées en matériaux qui serviront à la création de composants électroniques : purification et fonte du cuivre, de l’or, du platine, des métaux rares, etc. La production de certains composants cruciaux pour le numérique mobilise des capitaux et des savoirs titanesques, liant enjeux géopolitiques et monopoles économiques.

![Unité de fabrication de semi-conducteurs (chambre blanche)](media/bosch_waferfab-reutlingen_0620166096x3430_img_h720.jpg)

Par exemple, la production des semi-conducteurs, composants fondamentaux de toute l’infrastructure numérique et de ce qu’on appelle la *high-tech*, est structurée autour de pays et de groupes bien particuliers. Certaines entreprises conçoivent les puces (*fabless*), d’autres les fabriquent (*fab*), certains font un peu des deux (IDM, pour *Integrated Device Manufacturer*), comme Intel par exemple. Il y a aujourd’hui trois fabricants principaux (les *fab*) : TSMC (Taïwan), GlobalFoundries (U.S.A.) et UMC (Taïwan). L’infrastructure numérique n’existerait pas aujourd’hui, ou telle qu’elle se pense demain, sans eux et leurs chambres blanches anti-vibrations où sont gravées des feuilles de semi-conducteurs. Il est très probable que chaque objet numérique autour de vous ait un ou des composants intégrants leurs semi-conducteurs. L’électronique et le numérique, n’existeraient pas sans ces composants et l’industrie qui les produit, notamment à Taïwan et aux États-Unis.

Ce composant de quelques millimètres est l’enchevêtrement de savoirs scientifiques, de savoirs législatifs et juridiques, de forces politiques, d’intérêts financiers et de stratégies de développement économique. Ceci nous montre bien que n’importe quelle infrastructure dépasse le simple descriptif “technique”. Un exercice global a été mené par Kate Crawford and Vladan Joler pour déterminer tous les systèmes impliqués dans la fabrication, l’usage et le traitement en fin de vie d’un Amazon Echo. 

La cartographie qui en résulte, “Anatomy of an AI System”, témoigne bien de la complexité du numérique.[^20]

{{< aside >}}
#### Semi-conducteurs, transistors et circuits intégrés
Un semi-conducteur est un matériau qui peut être soit isolant ou soit conducteur. Cela permet de piloter le passage d’un signal électrique. Le transistor est un composant qui utilise un semi-conducteur afin de piloter la tension électrique, de moduler un signal et d’effectuer une opération logique. Un circuit intégré est une tranche de silicium sur laquelle on dispose de nombreux transistors. On arrive aujourd’hui à concevoir des transistors de plus en plus petits, augmentant la densité de transistors sur une même tranche de silicium. Par exemple, en 1971, un microprocesseur grand public comptait 2300 transistors. En 2017, un microprocesseur peut disposer de 19,2 milliards de transistors[^21].

![](media/encadre3.png)

La variation d’un semi-conducteur entre isolant et conducteur permet de définir un “état” (0/1) et donc d’effectuer une opération logique (“et”, “ou”, etc.). Ces opérations logiques permettent de traiter le signal, de stocker une information, d’opérer des calculs complexes et d’avoir de la mémoire.
{{< /aside >}}<span hidden>[^21]</span>

![Exportations de circuits intégrés électroniques par pays en 2016, selon la classification commerciale HS4](media/schema4@double-page.png)

#### Les centres de données
Les données numériques sont distribuées et stockées dans des centres de données à travers le monde. Ces centres peuvent être décrits comme de grands entrepôts, généralement situés en zone périurbaine, proches des lignes d’approvisionnement électrique et des grandes lignes de passage de la fibre. Ces entrepôts hébergent des rangées de serveurs qui stockent, calculent ou transfèrent des données. On y trouve aussi des équipements réseaux pour les opérateurs télécom et pour l’interconnexion avec les autres centres et réseaux. Les centres de données utilisent de l’électricité pour alimenter les serveurs, refroidir les salles et pour toutes les opérations annexes du centre (éclairage, surveillance, etc.)

Il existe quatre grands types de centre de données[^22] : 
- Les centres de données d’exploitation, directement placés dans les entreprises et les administrations qui opèrent leurs propres serveurs;
- Les centres de données commerciaux (hébergement/colocation), qui hébergent les serveurs d’autres entreprises ou mettent à disposition leurs serveurs en fonction des besoins de leurs clients, à travers le *cloud* par exemple;
- Les centres de données “Hyperscale”, qui sont des centres de données géants opérés principalement par les géants du numérique comme les GAFAM (Google, Apple, Facebook, Amazon, Microsoft);
- Les centres de données “High Power Computing” (HPC), qui sont des centres ultra-performants utilisés pour la recherche scientifique.

Ces centres de données ne sont pas répartis de façon homogène sur l’ensemble des territoires. Cela dépend bien évidemment du niveau de numérisation d’un territoire donné, de ses capacités d’investissement, des enjeux financiers et politiques inhérents à cette infrastructure. Par exemple,  les centres de données en colocation en 2018 sont au nombre de 2208 aux États-unis, 419 au Royaume-Uni, 262 en France, encore moins en Chine et en Russie. Les États-Unis possèdent une infrastructure numérique colossale pour ce qui est des centres de données. De fait, beaucoup de données du monde entier transitent par ceux-ci, avec tous les problèmes que cela pose en termes de sécurité, de monopole et de gouvernance des données. Cependant certains acteurs, comme la Chine, possèdent des centres de données beaucoup plus grand mais disposent *a priori* d’une offre de colocation moins large.

Un centre de données a besoin d’un espace, existant ou à construire, pour s’installer. À Paris les anciens bâtiments de l’industrie textile sont populaires car leur structure métallique peut supporter le poids des serveurs et des équipements réseaux. L’autre option consiste à construire un centre de données grâce à un permis de construire délivré par les autorités compétentes. Le choix de l’emplacement dépend de deux facteurs principaux : l’approvisionnement électrique et la connectivité. En France les centres de données sont généralement répartis autours des postes de distribution de ENEDIS ou parfois de RTE. Il arrive aussi que les opérateurs de centres de données construisent leurs propres postes de distribution électrique afin de se raccorder au réseau.

{{< aside >}}
#### Entrées et sorties d’un centre de données

![](media/encadre4.png)

{{< /aside >}}

Pour assurer leur connectivité (vitesse, redondance et qualité du réseau), les centres de données sont aussi installés près des dorsales d’internet, c’est-à-dire les axes principaux du réseau internet. Les centres de données de “Plaine Commune” ont été installés dans le nord de Paris car ils sont idéalement situés le long du réseau électrique et par rapport à un grande axe de connectivité d’internet qui longe l’A1. Les nouveaux centres de données de Saclay se situent aussi près d’une dorsale le long de l’A10. Il n’existe cependant pas de carte officielle des dorsales d’internet en France notamment pour des raisons de sécurité. Au-delà de l’électricité et de la connectivité un centre de données peut aussi avoir besoin d’eau pour son refroidissement. Les gestionnaires californiens de centres de données indiquent que certains de leurs centres peuvent utiliser jusqu’à 1 600 000 litres d’eau par jour pour leur refroidissement[^23]. La plupart des centres de données disposent également de générateurs et de cuves de fioul pour anticiper une coupure d’alimentation.

![Entrée du centre de données “Digital Realty” à Plaine Commune, Paris](media/15050hq.jpg "Yann Mambert")

L’arrivée d’un centre de donnée sur un territoire n’est donc pas anodine, elle correspond à un choix minutieux d’approvisionnement électrique et de connectivité afin de réduire les coûts et assurer la fiabilité du service. Une fois installé, un centre de données réserve une partie de la puissance électrique d’un territoire, parfois une partie d’un flux d’eau en même temps qu’il rejette plus ou moins de chaleur et influence le marché foncier. Tous ces phénomènes sont bien souvent invisibles au grand public mais les impacts électriques, matériels, environnementaux et fonciers de cette partie de l’infrastructure sont bien réels.


#### Les “Points d’Échange Internet” (IXP)
Pour faire transiter les flux de données entre les différents réseaux (fournisseurs d’accès internet, fournisseurs de services, etc.) des “Points d’Échange Internet” (*Internet eXchange Points* ou IXP) servent de carrefours et de raccourcis. Ces points sont cruciaux car ils permettent des interconnexions directes entre les différentes acteurs du réseau, évitant aussi de faire allers/retours complets d’un centre de données à l’autre. À l’échelle individuelle, c’est le fournisseur d’accès internet (FAI) qui permet à son client d’avoir accès au réseau internet. En théorie, un client particulier passe toujours par le centre de données de son FAI pour accéder au contenu souhaité stocké sur un autre serveur distant. Pour réduire cette distance les “Points d’Échange Internet” permettent des interconnexions d’un réseau à l’autre sans passer obligatoirement par les centres de données des fournisseurs d’accès et les plus grands centres de distribution du trafic. Cela permet aussi aux fournisseurs d’accès internet de transférer directement leur trafic d’un réseau à l’autre. 

Une entreprise distributrice de contenu intègre un IXP pour ouvrir une ligne directe vers son réseau, plus il y a d’entreprises participent à un IXP plus le nombre d’interconnexions directes augmente et le réseau gagne en rapidité et en fiabilité tout en réduisant les coûts. Les coûts d’exploitation d’un IXP sont généralement mutualisés entre les sociétés qui intègrent la structure. Le “Point d’Échange Internet” “Paris IX” compte par exemple plus de 300 membres de toute taille comme Apple, Blizzard, Arte, Blablacar, Bouygues, Orange, etc[^24]. Avoir ces raccourcis entre les différentes centres de données permet de réduire la latence, la bande passante et les coûts car la donnée voyage moins loin et mieux. Le but d’un IXP est de garder le trafic local à l’échelle locale (*keeping local traffic local*).

Au même titre que les centres de données, la distribution des IXP n’est pas homogène en fonction des géographies et des réseaux : les États-Unis en comptent 241 sur leur territoire, 35 pour la France et 106 pour les Pays-bas. Les modèles économiques changent radicalement en fonction de ces régions, les IXP nord-américains sont généralement des sociétés à profit alors que la scène européenne s’est structurée autour d’un modèle à but non lucratif. De ce fait les IXP européens fournissent des données plus précises et plus ouvertes que leurs homologues nord-américains. 

{{< aside >}}
#### Les “Points d’Échange Internet”, carrefour et raccourcis
Le chemin le plus court entre A et B passe par l’IXP si le réseau de A et de B y sont connectés.
À défaut de connexion directe en A et B, un chemin est possible par les fournisseurs d’accès qui sont connectés entre eux et à l’IXP.

![](media/encadre5.png)

{{< /aside >}}

Les Points d’Échange Internet proposent un tissu d’interconnexions de plus en plus dense qui rivalise avec les centres de données des fournisseurs d’accès internet qui fournissaient traditionnellement ce service. Les IXP sont des composants essentiels d’Internet souvent sous-estimés. Leur présence permet d’optimiser le transfert de données et d’économiser les coûts électriques et financiers du transfert de données sur les réseaux.


#### Les câbles sous-marins
Maintenant que nous connaissons la distribution des centres de données dans le monde et leurs interconnexions locales grâce aux IXP,  nous devons nous poser la question des envois de données intercontinentaux et à longue distance. Comment une donnée stockée sur un serveur dans un centre de données au Nevada arrive t-elle sur un terminal en France ? Comment cette donnée traverse t-elle l’océan en moins d’une seconde ? Elle partira du centre de données, passera par un point d’atterrissage de câble (*cable landing point*), c’est-à-dire un lieu sur la côte où un câble sous-marin fait surface. Elle traversera ce câble, arrivera au point d’atterrissage de l’autre côté de l’Atlantique, passera sûrement par un IXP et sera reçu sur le terminal. Un câble de 7000 kilomètres (distance moyenne entre un point d’atterrissage côte est U.S. et un point d’atterrissage français) permet l’intercommunication entre deux continents. Quelques questions se posent d’emblée pour bien comprendre cette partie de l’infrastructure : de quoi sont faits ces câbles et comment sont-ils installés ? Finalement, qui paye et qui en détient la propriété ? 

Il y avait 406 câbles sous-marins dans le monde au début de l’année 2020[^25], pour une longueur cumulée de 1,2 million de kilomètres. Ils permettent un débit plus ou moins important en fonction de leur âge de déploiement. Par exemple le FLAG Atlantic-1 (2000) qui relie Island Park, NY, à Plerin en Bretagne a un débit de 2x2.4 Tbps (Terabytes par seconde). Le nouveau câble MAREA (2018) a une capacité de débit de 208 Tbps, soit 43 fois plus de débit. À l’intérieur de ces câbles de 10 centimètres de diamètre, se trouvent plusieurs fibres optiques enrobées de caoutchouc, parfois de kevlar. Ils sont déposés au fond de l’océan par des navires spécialement conçus pour cette mission appelés “câbliers”. Ces navires permettent le déploiement mais aussi la réparation des câbles pour les télécommunications et le réseau énergétique. Les câbles ont une durée de vie minimum de 25 ans et peuvent rester opérationnel plus longtemps. Toutefois ils sont souvent remplacés plus tôt par des câbles plus récents avec une meilleure capacité de débit qui sont plus intéressant en termes économiques.

Pour que des données traversent des milliers de kilomètres via une fibre optique le signal est répété par un laser dans un point d’atterrissage. Ces point d’atterrissage sont généralement des bâtiments assez simples sur la côte avec quelques bureaux et les équipements réseaux nécessaires à la gestion du trafic. Par exemple, le signal lumineux envoyé par le laser d’un point d’atterrissage en France est reçu  de l’autre côté de l’Atlantique par son vis-vis étatsuniens. Celui-ci le distribue ensuite sur le réseau internet nord-américain jusqu’à destination. Cette opération dure moins d’une seconde.

![Section d’un câble sous-marin, la fibre optique est au milieu du câble](media/201405_section_cable_fibre_optique_sous-marin.jpg "FOP 2011")

Les pannes sont généralement provoquées soit par un navire qui pose l’ancre sur le câble soit par de la pêche en eaux profondes si le filet embarque le câble. On dénombre en moyenne 100 accidents par an. Certains pays disposent de plusieurs câbles pour assurer leur connectivité intercontinentale, toutefois d’autres pays ne sont reliés à Internet que par un seul câble. Un dégât sur ce câble implique alors une coupure nationale de l’accès à Internet. En 2017, le câble reliant Madagascar à l’Afrique du Sud a été sectionné suite un séisme, provoquant une coupure au réseau Internet pendant 12 jours. En 2015, une section de câble a privé l’Algérie d’Internet pendant deux semaines.

![Navire câblier Skagerrak](media/SubmarineCableBoat.jpg "Nexans")

![Des plongeurs enlèvent des anodes de zinc corrodées sur un câble sous-marin près d’Hawaii](media/underseacable.jpg "Flickr/Official U.S. Navy Page")

Le déploiement d’un câble nécessite un capital financier énorme, aux alentours d’un milliard de dollars pour un câble transatlantique. Généralement des consortiums se forment pour co-investir et partager la propriété d’un câble. Ces dernières années, de nouveaux acteurs sont arrivés dans ce secteur, les GAFAM ont largement investis dans les nouveaux projets de câbles pour répondre à leur gigantesque besoin de trafic et pour prioriser le déploiement de routes dont ils ont besoin par rapport à leur stratégie.

![The “Physical Internet” montre la répartition des centres de données, d’IXP et de câbles sous-marins dans le monde](media/physical-internet-corretta@double-page.jpg "B. Bazzan, A. Colombo, M. Giordano, G. Misto, L. Piro, I. Stojsic")

#### Les antennes
L’infrastructure numérique compte sur de nombreuses antennes pour parcourir le premier ou le dernier kilomètre quand la fibre ne le peut pas, notamment dans le cas du trafic mobile via les smartphones. En effet, lorsqu’un smartphone se connecte à internet son premier point de contact est une antenne 2G/3G/4G/5G. Une antenne est un équipement réseau qui diffuse un signal à une certaine fréquence, comme une tour radio. Un smartphone se synchronise à cette fréquence pour envoyer des données et à une autre pour recevoir des données. Une antenne est généralement fixée à un support, c’est-à-dire, une tour qui sert d’arrimage à plusieurs antennes et qui est raccordé au réseau électrique et au réseau fibre. Chaque antenne, ou le support d’antenne, est reliée à une fibre optique qui part ensuite en souterrain ou en aérien (attachée à des poteaux dans la rue). Cette fibre part ensuite jusqu’à un centre de données capable de traiter la demande ou via un IXP. Il faut donc retenir qu’une antenne est généralement égale à un raccordement à la fibre, il n’y a donc que la connexion entre le smartphone et l’antenne qui est sans-fil.

![Site de transmission dans le Cantal (support, stations, antennes 2G, 3G, 4G et faisceau hertzien)](media/stationMassiac.jpg "Gauthier Roussilhe")

On distingue 3 types d’équipements sans-fil : le support, la station et l’antenne. Le support est l’élément physique qui sert d’attache aux stations et donc aux antennes. Une station indique la présence des équipements d’un opérateur sur un support. Une antenne fait partie de la station déployée par un opérateur. En fonction du nombre d’opérateurs, un support peut servir à plusieurs stations. Chaque support est raccordé au réseau filaire et au réseau électrique. Les stations vont utiliser de l’électricité pour diffuser un signal en continu, traiter des requêtes et recevoir/envoyer des données via leurs antennes. De la puissance électrique est aussi utilisé pour la climatisation de l’installation car les stations sont situées sur des toits ou des points élevés non ombragés et produisent de la chaleur. Côté usager, un smartphone consomme plus d’électricité lorsqu’il fonctionne en réseau cellulaire (3G/4G) plutôt qu’en WiFi. En effet, la distance à parcourir est généralement moins longue via la WiFi car il s’agit d’atteindre la box la plus proche dans un foyer ou un lieu professionnel.

En 2020, on compte 46 685 supports, 85 532 stations et 223 480 antennes en France, tous opérateurs et fréquences confondus[^26]. Chaque territoire n’est pas couvert de la même façon. Les zones urbaines sont logiquement plus couvertes car elles concentrent plus de trafic. On y trouve plus de stations et d’antennes par habitant et au km2 car la densité de population est plus élevée et aussi les usages numériques urbains sont plus intenses en données, comme le visionnage de vidéos dans les transports en commun. On compte à Paris (75) 17 supports, 23 stations et 61 antennes par km2 (superficie totale de 105km2), ainsi que 0,8 support, 1 station, et 2,8 antennes pour mille habitants (2 273 305 habitants en 2013). Les zones urbaines denses impliquent donc une densité de supports au km2 mais cela n’implique pas forcément un déploiement dense par rapport au nombre d’habitants. À titre de comparaison, la Lozère (48) accueille 0,03 support, 0,07 station et 0,16 antenne par km2 (superficie de 5 176km2). Rapporté à la population, il y a 1,8 support, 4,9 stations et 10,7 antennes pour mille habitants (77 085 habitants en 2013).

<table id="tableau1">
    <caption>Comparaison de l’infrastructure réseau entre l’Île de France et la Lozère</caption>
    <thead>
      <tr>
        <th></th>
        <th class="text-center">Paris (75)</th>
        <th class="text-center">Lozère (48)</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Support/km²</td>
        <td class="text-right">17,238</td>
        <td class="text-right">0,028</td>
      </tr>
      <tr>
        <td>Station/km²</td>
        <td class="text-right">23,267</td>
        <td class="text-right">0,074</td>
      </tr>
      <tr>
        <td>Antenne/km²</td>
        <td class="text-right">61,448</td>
        <td class="text-right">0,074</td>
      </tr>
      <tr>
        <td>Support/mille hab.</td>
        <td class="text-right">0,796</td>
        <td class="text-right">1,881</td>
      </tr>
      <tr>
        <td>Station/mille hab.</td>
        <td class="text-right">1,075</td>
        <td class="text-right">4,930</td>
      </tr>
      <tr>
        <td>Antenne/mille hab.</td>
        <td class="text-right">–</td>
        <td class="text-right">10,754</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td>Notes</td>
        <td class="text-right">105 km² <br>2 273 205 hab. <br>(2013)</td>
        <td class="text-right">5 167 km² <br>77 085 hab. <br>(2013)</td>
      </tr>
    </tfoot>
</table>

La couverture effective d’un territoire pour le trafic mobile (support/station/antenne) doit toujours être comprise par rapport à la superficie totale à couvrir et à la densité de population. Ce n’est pas parce qu’il y a plus de support par habitant en Lozère qu’il y a plus de support au km2. Cela est due au fait que la population en Lozère est en moins importante et beaucoup plus dispersée qu’à Paris.
En dernier lieu, la portée maximum d’une antenne varie en fonction du débit de celle-ci. Plus une antenne a de la portée moins elle permet de transporter d’information. Par exemple les antennes 5G dernière génération permettent un débit de 1Gb/s mais n’ont une portée que de 150 mètres en moyenne. Ces antennes peuvent avoir leur place dans un milieu urbain très dense mais ne sont pas pertinentes dans un territoire où la population est très dispersée. De façon générale, la dernière génération de réseau mobile (5G) propose des applications professionnels et grand public mais pose aussi de nombreuses questions sur sa viabilité énergétique, environnementale et économique. Un rapport du même auteur que ce document, intitulé “La controverse de la 5G” traite du sujet[^27].

Pour résumer, dans l’infrastructure numérique les réseaux mobiles servent généralement à parcourir le dernier kilomètre séparant un appareil mobile du réseau général. Le trafic “capturé” par les antennes/stations part ensuite dans le réseau fibré, comme le trafic fixe, pour atteindre le coeur de l’infrastructure, c’est-à-dire le centre de données.

#### Les équipements utilisateurs
Notre vie quotidienne se compose aujourd’hui de nombreux appareils connectés : smartphones, ordinateurs, box internet, télévisions, montres connectées, consoles de jeu vidéo, enceintes connectées, voitures, thermostat connecté; frigo connecté, etc. Le fonctionnement de chaque de ces appareils implique le transfert de données sollicitant des centres de données et le réseau, en utilisation comme en veille. Il y a aussi tous les objets et capteurs connectés utilisés dans le milieu professionnel, on peut penser aux caméras de surveillance, aux capteurs industriels, aux feux de signalisation, aux navettes autonomes, à certains types de tracteurs, etc. La caractéristique principale de ces équipements est leur volume. En 2019, l’écosystème numérique compterait 37 milliards d’équipements informatiques, dont 3,5 milliards de smartphones, 3,8 milliards d’autres téléphones, 3,1 milliards de télévisions/écrans/vidéoprojecteurs et à peu près 19 milliards d’objets connectés[^28]. Le reste de ces équipements est regroupé autour des réseaux (antennes, box) et des centres informatiques. Une telle masse d’équipements témoigne donc de l’arrivée massive des objets et capteurs connectés dont on estime qu’ils pourraient représenter à eux seuls un volume de 50 milliards d’ici 2025. Hors objets connectés, la progression des autres équipements stagne car les marchés sont saturés dans les pays fortement intégrés à l’infrastructure numérique. De nombreuses entreprises misent sur les pays “émergents” pour écouler leurs stocks d’appareils.

Dans cette galaxie d’appareils le smartphone est sûrement l’emblème de l’infrastructure numérique contemporaine. Il est devenu un appareil incontournable pour de nombreuses personnes quelque soit le pays. On l’utilise pour consulter son compte en banque, payer, gérer ses emails, regarder des films, lire les nouvelles, pour payer les transports en commun et bien d’autres usages. En 2020, il y aurait entre 4 et 4,5 milliards d’utilisateurs d’internet dans le monde[^29] dont 91% utiliseraient un smartphone, soit entre 3,6 et 4 milliards d’utilisateurs actifs de smartphones. Les livraisons mondiales de smartphones étaient de 1,389 milliards d’unités en 2018 et 1,366 milliards en 2019. Depuis 2015, les ventes annuelles se sont stabilisées aux alentours de 1,5 milliards de smartphones : le marché des smartphones est aujourd’hui en saturation.

<table id="tableau2">
    <caption>Livraisons mondiales de smartphones (en millions) (Canalys Smartphone Market Pulse 2019)</caption>
    <thead>
      <tr>
        <th class="text-center">Vendeur</th>
        <th class="text-center">Livraisons 2018</th>
        <th class="text-center">Livraisons 2019</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Samsung</td>
        <td class="text-right">293,3</td>
        <td class="text-right">298,1</td>
      </tr>
      <tr>
        <td>Huawei</td>
        <td class="text-right">206</td>
        <td class="text-right">240,6</td>
      </tr>
      <tr>
        <td>Apple</td>
        <td class="text-right">212,2</td>
        <td class="text-right">198,1</td>
      </tr>
      <tr>
        <td>Xiaomi</td>
        <td class="text-right">120,6</td>
        <td class="text-right">125,5</td>
      </tr>
      <tr>
        <td>Oppo</td>
        <td class="text-right">116</td>
        <td class="text-right">120,2</td>
      </tr>
      <tr>
        <td>Autres</td>
        <td class="text-right">441,4</td>
        <td class="text-right">384,3</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td>Total</td>
        <td class="text-right">1389,4</td>
        <td class="text-right">1366,7</td>
      </tr>
    </tfoot>
</table>

Sait-on combien coûte la production d’un équipement numérique comme un smartphone ? De plus, sait-on combien de ressources (eau, énergie, minerais, etc.) sont nécessaires à la production d’une telle masse d’équipements ? Pour déterminer les coûts des composants d’un appareil on utilise une “nomenclature” ou ​Bill of Materials​ (BoM) en anglais. Cette nomenclature permet de déterminer le prix de chaque composant et le poids de chaque composant dans la facture de production totale. Par exemple, le coût de l’écran d’un iPhone 7 représentait 17,7% du coût total des composants. Pour un iPhone X, l’écran représentait 29,7% du coût total des composants. Avec chaque nouvelle génération de smartphones la taille des écrans augmente et le coût de l’écran représente une part de plus en plus importante de la nomenclature.

<table id="tableau3">
    <caption>Comparaison de nomenclature ($/%) entre un iPhone X et un iPhone 7 (IHS Markit)</caption>
    <thead>
      <tr>
        <th class="text-center">Estimation BoM</th>
        <th class="text-center" colspan="2">iPhone X</th>
        <th class="text-center" colspan="2">iPhone 7</th>
      </tr>
      <tr>
        <td></td>
        <td class="text-right">Coût ($)</td>
        <td class="text-right">% (BoM)</td>
        <td class="text-right">Coût ($)</td>
        <td class="text-right">% (BoM)</td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="text-right">Écran tactile</td>
        <td class="text-right">110</td>
        <td class="text-right">29,7</td>
        <td class="text-right">39</td>
        <td class="text-right">17,7</td>
      </tr>
      <tr>
        <td class="text-right">Éléments électromécaniques</td>
        <td class="text-right">61</td>
        <td class="text-right">16,5</td>
        <td class="text-right">34,9</td>
        <td class="text-right">15,8</td>
      </tr>
      <tr>
        <td class="text-right">Caméra</td>
        <td class="text-right">35</td>
        <td class="text-right">9,5</td>
        <td class="text-right">19</td>
        <td class="text-right">8,6</td>
      </tr>
      <tr>
        <td class="text-right">Processeur</td>
        <td class="text-right">27,5</td>
        <td class="text-right">7,4</td>
        <td class="text-right">26,9</td>
        <td class="text-right">12,2</td>
      </tr>
      <tr>
        <td class="text-right">Système RF</td>
        <td class="text-right">18</td>
        <td class="text-right">4,9</td>
        <td class="text-right">33,9</td>
        <td class="text-right">15,4</td>
      </tr>
      <tr>
        <td class="text-right">CI Alimentation</td>
        <td class="text-right">14,25</td>
        <td class="text-right">3,8</td>
        <td class="text-right">7,2</td>
        <td class="text-right">3,3</td>
      </tr>
      <tr>
        <td class="text-right">Mémoire</td>
        <td class="text-right">33,45</td>
        <td class="text-right">9</td>
        <td class="text-right">16,4</td>
        <td class="text-right">7,4</td>
      </tr>
      <tr>
        <td class="text-right">RF/Amplificateur</td>
        <td class="text-right">16,6</td>
        <td class="text-right">4,5</td>
        <td class="text-right">7,2</td>
        <td class="text-right">3,3</td>
      </tr>
      <tr>
        <td class="text-right">CI Interface</td>
        <td class="text-right">12,4</td>
        <td class="text-right">3,3</td>
        <td class="text-right">14</td>
        <td class="text-right">6,3</td>
      </tr>
      <tr>
        <td class="text-right">Capteur "TrueDepth"</td>
        <td class="text-right">16,7</td>
        <td class="text-right">4,5</td>
        <td class="text-right">0</td>
        <td class="text-right">0</td>
      </tr>
      <tr>
        <td class="text-right">WLAN / Bluetooth</td>
        <td class="text-right">7,35</td>
        <td class="text-right">2</td>
        <td class="text-right">8</td>
        <td class="text-right">3,6</td>
      </tr>
      <tr>
        <td class="text-right">Batteries</td>
        <td class="text-right">6</td>
        <td class="text-right">1,6</td>
        <td class="text-right">2,5</td>
        <td class="text-right">1,1</td>
      </tr>
      <tr>
        <td class="text-right">Contenu boîte</td>
        <td class="text-right">12</td>
        <td class="text-right">3,2</td>
        <td class="text-right">11,8</td>
        <td class="text-right">5,3</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td>Total</td>
        <td class="text-right">370,25</td>
        <td class="text-right">100</td>
        <td class="text-right">220,8</td>
        <td class="text-right">100</td>
      </tr>
    </tfoot>
</table>

Pour produire des composants il faut de la matière transformée (métaux, plastique, etc.). Quelle part représente ces matières dans le prix total d’un smartphone ? Une étude anglaise publiée en 2015 estimait que pour un téléphone de £600 l’achat des matières brutes correspond à £1,5, soit 0,25% du prix de vente[^30]. Cela représente théoriquement la part de l’exploitation minière puisque c’est elle qui vend la matière brute. Ce chiffre peut varier en fonction des modèles et des composants. La course à la miniaturisation crée une demande sur des métaux plus rares ou plus compliqués à extraire, ce qui augmente leur prix. On estime qu’un smartphone intègre entre 45 et 50 métaux différents, dans des proportions très différentes, en plus des autres matériaux usuels (plastiques, caoutchouc, etc.). L’entreprise de smartphones “Fairphone” a mis à disposition une carte interactive qui permet de voir l’ensemble de ses fournisseurs, des minerais aux plastiques[^31]. L’entreprise compte à peu plus de 110 fournisseurs répartis sur plus de 30 pays. Pour comparer à un autre fabricant, on estime que la fabrication d’un iPhone nécessite une chaîne d’approvisionnement impliquant 43 pays.

Parmi les nombreux minerais qui composent un smartphone, certains sont extraits dans des conditions très difficiles, à l’encontre des droits humains et environnementaux. Des exploitations minières sont parfois contrôlés par des seigneurs de guerre ou des cartels locaux qui emploient hommes, femmes et enfants. Au même titre que l’on parle des “diamants de sang” on parle de “minerais de conflits” (​conflict minerals​). Fairphone a essayé de choisir des fournisseurs qui ne sont pas impliqués dans ce genre de pratiques sur 10 métaux critiques parmi les 38 qui composent leur smartphone : cobalt, cuivre, gallium, or, indium, nickel, terres rares, tantale, étain et tungsten. Toutefois, de l’aveu de l’ancien PDG de l’entreprise il n’est pas possible de faire un smartphone “juste” (​fair​) mais seulement un smartphone un peu plus juste (fairer​) car les chaînes d’approvisionnement de beaucoup de fournisseurs sont opaques et il est parfois impossible de connaître la provenance de certains matériaux et les conditions d’extraction.

![Comparaison de la nomenclature (fabrication incluse) et du prix de vente des différents modèles d’iPhones](media/schema5@double-page.png)

La vie d’un équipement utilisateur ne s’arrête pas à la production et à l’usage, il y a aussi la fin de vie. Que se passe t-il lorsqu’on amène sa télévision à la décharge ou lorsqu’on jette son smartphone ? En 2016, On estime que 44,7 millions de tonnes métriques de déchets d’équipements électriques et électroniques (DEEE) ont été générées. Cela représente en une année une masse équivalente à 4 500 tours Eiffel ou 6,1 kilos par habitant[^32]. Cette masse d’équipements en fin de vie continue à augmenter à cause du renouvellement trop rapide des équipements. Dans le cas du smartphone, on a estimé sa durée de vie à 21,6 mois en 2015, soit moins de 2 ans[^33]. Cela est lié aux campagnes marketing qui encourage ce renouvellement, mais aussi lié aux différents types d’obsolescence (logicielle, matérielle, psychologique) et de la masse de livraisons annuelles à écouler. Les métaux et les composants de ces équipements sont peu, voire très peu recyclés sauf pour certains cas comme le cuivre sans que ce recyclage implique la fin de l’extraction minière du cuivre. Beaucoup de cuivre est immobilisé dans le bâti et les infrastructures et ne repart dans les circuits de production.

Ce qui caractérise fondamentalement les équipements numériques, personnels comme professionnels, c’est leur masse. Nous produisons des milliards d’équipements chaque année stimulant des chaînes d’approvisionnement très complexes et parfois opaques, notamment dans le cas de l’extraction minière. Le renouvellement forcé des équipements pose des problèmes considérables de gestions des déchets ou de réutilisation des équipements considérés comme “obsolètes”. À ce titre les équipements utilisateurs sont à la fois le début et la fin des infrastructures numériques, de la mine à la décharge, avec seulement quelques années d’usage entre temps. Ces flux et ces systèmes posent de nombreux questions que nous étudierons dans la partie sur les impacts environnementaux.

### Les tendances de développement du numérique

Les infrastructures et les usages du numérique continuent à évoluer et sont sujets à de nombreuses interprétations sur leur développement futur. Ces exercices de projection servent notamment à tirer des courbes et des tendances pour rendre les futurs plus certains et adapter dès aujourd’hui les modèles de développement. Toutefois, le numérique n’est pas une infrastructure isolée et elle est elle-même influencée par d’autres systèmes, d’autres infrastructures, d’autres idées : l’extraction de ressources, la production d’énergie, l’idée de croissance économique et de progrès technologique, etc. Il est alors important de comprendre comment les sociétés numérisées de 2020 définissent le développement du numérique dans les années et les décennies à venir : quels usages, quels secteurs, quels facteurs, quels imaginaires favorisent-elles pour produire les futurs souhaités par les grands acteurs du numérique ?

#### Le trafic
Une des premières données que l’on projette dans le futur est sûrement le trafic. Aujourd’hui, on ne voit qu’une seule évolution possible : son augmentation. Cisco rapporte que le trafic internet global en 2017 était de 1,2 ZT (Zettabyte). Pour ramener ce chiffre a une valeur plus connue comme le GB (Gigabyte), 1,2 ZB est égal à 1 117 587 089 538 GB, soit 1 117 milliards de GB. Cela implique qu’à peu près 127 000 000 GB circulent en moyenne chaque heure. Cisco estime qu’en 2022 le trafic global atteindra 4,2 ZB, soit une multiplication par 3,5 en 5 ans[^34]. Ces estimations sont à peu près partagées par tous les acteurs de l’infrastructure numérique. Cette augmentation s’explique avec 4 facteurs : une plus grande part de la population mondiale utiliserait internet dans une avenir proche, 4,8 milliards en 2022 contre 3,5 milliards en 2017; l’augmentation du taux d’équipement numérique par habitant, 2,4 équipements par personne en 2017 contre 3,6 en 2022; l’augmentation de la vitesse de transfert et finalement l’augmentation de l’usage vidéo, 75% du trafic en 2017 contre 82% en 2022.

Ces facteurs n’offrent cependant qu’une vue partielle pour comprendre l’évolution du trafic, car une question importante se pose : au-delà de l’augmentation, est-il possible de “piloter” l’évolution du trafic ? Cette question est importante car une grande partie des hypothèses de développement du numérique se base sur une augmentation constante sans jamais soulever l’hypothèse d’un pilotage en vue d’une stabilisation ou même d’une baisse.

![Évolution du trafic internet mondial entre 1997 et 2017 et projection pour 2022](media/schema7@double-page.png)

Par exemple, la nature des forfaits mobiles, auxquels nous souscrivons tous, influe sur notre consommation de données. Un forfait illimité favorise une consommation croissante de données. De même, un réseau télécom qui offre une très bonne qualité de signal, un bon débit et une bonne couverture est nécessaire à la consommation de services plus lourds en données, comme une vidéo par exemple. Recevoir un signal de bonne qualité, calculer des opérations complexes, lire une vidéo en très haute définition, travailler sur des fichiers de façon synchronisée avec d’autres personnes nécessitent des appareils puissants, cela vaut pour les ordinateurs comme pour les smartphones. Obtenir un bon signal implique aussi des composants de qualité (antenne, etc.). De plus, les caractéristiques des équipements influent aussi sur le trafic : ​a priori​ je ne peux pas me connecter au réseau 4G si mon smartphone ne dispose pas d’un émetteur/récepteur compatible 4G. Finalement la nature des services numériques proposés sur le réseau est sûrement le principal facteur d’évolution du trafic. Une grande partie des services numériques populaires aujourd’hui sont souvent “intensifs” en données, c’est-à-dire, qu’ils font transiter une masse relativement importante de données par minute d’utilisation. Cette intensité peut être liée aux types de média distribués, les vidéos sont les formats grand public les plus lourds en données, et aussi aux méthodes de captation des données et de l’attention.

Dans le trafic internet mondial le trafic mobile représente une part de plus en plus significative. Le gestionnaire et équipementier de réseau Cisco estime que le trafic mobile atteindra 77 EB par mois en 2022[^35], soit une multiplication par 7 de ce trafic depuis 2017 (12 EB par mois). En suivant cette projection le trafic mobile s’élèverait donc à 924 EB par an en 2020. Ce chiffre représente presque l’ensemble du trafic fixe et mobile en 2017 et à peu près un quart du trafic global estimé en 2022 par Cisco. Aujourd’hui la hausse du trafic mobile est largement souhaitée par les acteurs du numérique afin d’avoir accès à de nouveaux publics et donc à de nouveaux marchés, notamment dans les pays du Sud.

Aujourd’hui les différents grands acteurs du numérique présentent un avenir de l’infrastructure avec toujours plus de trafic. L’intensité en données des services numériques pèsent aujourd’hui de tout son poids pour influencer le trafic à la hausse. Tous les autres acteurs semblent aujourd’hui s’adapter à cette situation bien que la relation soit asymétrique. Pourtant les quatre indicateurs d’évolution du trafic montre qu’il y a bien des leviers de contrôle.

#### La recherche de débouchés
De nombreux acteurs s’efforcent de promouvoir la numérisation de pans entiers de l’économie. Certains espoirs sont notamment portés vers le secteur industriel, le secteur des transports et du bâtiment. Les scénarios de numérisation croissante de ces secteurs se traduisent généralement par l’installation massive de capteurs permettant la captation et l’analyse de données. On suppose alors que l’accumulation de données et leur traitement permettraient l’optimisation ou l’automatisation de processus industriels, de régulation de consommation divers (comme l’électricité) et l’usage de programmes apprenants (comme la conduite de véhicules par exemple). L’Agence Internationale de l’Énergie (IAE) a produit en 2019 un tableau pour essayer d’estimer la faisabilité et les impacts potentiels de la numérisation de ces secteurs, notamment par rapport à la demande énergétique globale. L’IAE a estimé que l’automatisation industrielle, l’optimisation des processus industriels ou l’usage de masse de données dans l’aviation étaient à la fois plausible sans que cela affecte considérablement la demande énergétique, toutefois les véhicules autonomes représentent un avenir peu plausible et un impact très fort sur la demande énergétique (à la hausse)[^36].

![L’impact potentiel de la numérisation sur les transports, les bâtiments et l’industrie](media/schema8@double-page.png)

Le secteur de l’énergie, particulièrement celui des énergies fossiles, semble aussi être un milieu attirant pour le secteur numérique. Amazon, Google, Microsoft sont connus pour avoir des branches dans le secteur de l’énergie, notamment pour gérer leur propre approvisionnement, mais aussi pour vendre des solutions “d’intelligence artificielle” et de traitement de masse des données aux compagnies pétrolières et gazières. Le marché global pour les solutions “d’intelligence artificielle” dans le secteur des énergies fossiles était estimé à 1,75 milliard de dollars en 2018 et pourrait atteindre 4,01 milliards de dollars en 2025, soit un taux de croissance annuelle de 12,5%[^37]. Dès 2017, les équipes commerciales de vente d’Amazon Web Services démarraient leur séminaire annuel intitulé “Positioning for Success in Oil & Gas” (se positionner avec succès dans le secteur du pétrole et du gaz)[^38] et Amazon Power vend ses services afin de trouver et de récupérer plus efficacement du pétrole ainsi que de réduire le coût par baril[^39]. Les grands acteurs du numérique estiment aussi qu’ils peuvent aider à faciliter l’usage d’énergies renouvelables et d’optimiser leur production[^40]. Toutefois ce double discours rappelle qu’il est toujours important de comprendre ces compagnies dans leur dimension globale en observant cas par cas leurs activités pour dépasser le simple effet de communication.

Finalement, une explosion des objets connectés est souhaitée par de nombreux acteurs du secteur numérique et industriel. Cette explosion est généralement associée à la question de la “smart city” qui désigne l’implémentation massive de capteurs pour gérer les flux d’une ville (trafic, énergie, voirie, déchets, eau, etc.) dans l’idée de les optimiser. Cette idée est aussi répétee dans le contexte de la “smart home” (maison intelligente). Dans une étude de 2017, certains analystes estimaient que 50 milliards d’objets connectés seraient actifs en 2030[^41]. Ce chiffre est à relativiser car Cisco prévoyait déjà dans une étude 2011 que 50 milliards d’objets connectés seraient déployés en 2020[^42]. Néanmoins, le marché global de l’Internet des Objets (IoT) était évalué à 193 milliards de dollars en 2019 et est estimé à 657 milliards d’ici 2025, soit une croissance annuelle de 21% pendant 5 ans[^43]. Il est généralement estimé que le marché des objets connectés pourrait représenter à terme la moitié du marché du secteur numérique. C’est donc un marché particulièrement disputé par de nombreux acteurs du numérique, entraînant une course à la numérisation de nombreux objets du quotidien (thermostat, montre, enceinte, frigo, etc.) et d’objets à vocation “industrielle” (compteur d’eau, compteur d’électricité, etc.).

![Pyramides des usages potentiels de la 5G](media/schema9.png)

Les pistes énoncées ici sont des scénarios proposés par différents acteurs qui ont des intérêts financiers à faire advenir ces propositions (industries, investisseurs, etc.). Ces scénarios servent notamment à créer des imaginaires pour interroger leur acceptation sociale et parfois pour créer du consentement. Ils n’évacuent toutefois pas la complexité derrière de tels déploiements : enjeux sociaux et démocratiques, impacts environnementaux, faisabilité technique, rentabilité financière. Il est important de se rappeler que les évolutions technologiques ne sont pas des révolutions où tout change d’un coup, ce sont des systèmes qui s’additionnent aux systèmes précédents et qui creusent un sillon petit à petit. Tous ces scénarios restent à éprouver pour considérer s’ils sont réalistes ou non, souhaitables ou non, notamment dans le cadre des transitions à opérer.

### Les impacts environnementaux du numérique

Pour considérer quels scénarios de développement du numérique sont possibles il est nécessaire dans un premier temps d’estimer leur soutenabilité à plusieurs niveaux. Comme nous l’avons vu dans les cahiers précédents l’infrastructure numérique sollicite de nombreux écosystèmes pour extraire les matières premières nécessaires à la production de composants. Elle s’appuie aussi sur des chaînes d’approvisionnement et de production globales afin de livrer des produits numériques partout dans le monde. Elle nécessite de l’énergie (pas seulement de l’électricité) à tous les niveaux, produisant plus ou moins de gaz à effet de serre. Finalement cette infrastructure entraîne aussi une production de déchets très peu recyclables ou non recyclés en aval.

Décrire tous les impacts environnementaux liés à la production de l’infrastructure numérique (fabrication des serveurs, antennes, équipements, etc.) et à son usage est un exercice complexe et qui varie grandement en fonction des équipements, des méthodes de production et des territoires concernés. Cette description des impacts ne peut être précise que si elle se concentre sur une production d’équipement et son usage à une échelle explicitement définie. Par exemple, quels sont les impacts environnementaux liés à la production, à l’usage et à la fin de vie d’un iPhone 6, quels sont les impacts environnementaux liés au visionnage d’une heure de vidéo HD sur Youtube via la 4G sur ce même iPhone. Ce type de démarche s’appelle une analyse de cycle de vie (ACV). Les ACV sont utilisées pour définir tous les impacts liés à un produit dans un périmètre donné. Dans le cadre d’un service numérique, cela consiste à définir une “unité fonctionnelle”, c’est-à-dire, l’usage d’une fonction d’un produit ou d’un service comme : regarder une heure de vidéo HD sur Youtube en 4G sur un iPhone 6 en France. Dans ce cas précis l’iPhone est le vecteur matériel d’un usage : regarder une vidéo hébergée sur un serveur distant. Une fois que nous avons sélectionné l’unité fonctionnelle de notre analyse, nous devons fixer les limites de l’analyse : est-ce que nous intégrons la production, l’usage et la fin de vie, ou seulement deux étapes, voire une ? Finalement, nous pouvons choisir les critères à étudier : émissions de gaz à effet de serre, consommation d’eau douce, extraction de ressources non-renouvelables (minerais, métaux, etc.), consommation d’énergie primaire (pétrole, gaz, charbon, nucléaire, énergies renouvelables, etc.), pollution des sols (contamination liée à l’extraction, fabrication, fin de vie). Il est toujours compliqué de déterminer les frontières d’un système et donc les frontières de l’analyse, notamment dans le numérique. Les personnes dont le métier est de faire des analyses de cycle de vie s’accordent généralement sur un modèle commun afin de permettre la validité et la comparaison de leurs résultats. Dans le cas du numérique le modèle commun consiste à étudier les impacts environnementaux à trois endroits distincts : les centres de données, les réseaux et les équipements utilisateurs.

{{< aside >}}
#### Les normes ISO pour les analyses de cycle de vie
Il existe des normes internationales qui fixent les méthodes et les exigences liées à l’analyse de cycle de vie, il s’agit des normes ISO 14040 et 14044. Elles sont les références communes à toutes les personnes qui effectuent des analyses de cycle de vie. Il est conseillé de suivre cette méthodologie afin que les résultats obtenus sont facilement exploitables et lisibles par ses consoeurs et confrères partout dans le monde.
{{< /aside >}}

#### Définir les impacts
Toute personne cherchant à définir les impacts environnementaux d’une action, d’un usage numérique va donc regarder à trois endroits différents mais quels impacts cherche t-on exactement ? L’infrastructure numérique a des impacts multiples sur les écosystèmes : utilisation d’eau douce, extraction de minerais dans des écosystèmes fragiles, pollutions des sols, pollutions en décharge des équipements, émissions de gaz à effet de serre.. Un indicateur retient particulièrement l’attention aujourd’hui car il correspond aux objectifs de transition énoncés par les institutions nationales et internationales : les émissions de gaz à effet de serre. Ces gaz sont généralement émis à la production des équipements informatiques (fioul pour l’extraction et le transport, énergie pour faire tourner les lignes de production industrielle, les fonderies, etc.) et à la production de l’énergie nécessaire pour faire fonctionner ces équipements (production d’électricité). Les émissions de gaz à effet de serre sont donc directement reliés à la consommation énergétique de l’infrastructure numérique, c’est-à-dire à la fabrication et à l’utilisation des équipements. Toutefois, malgré l’attention portée aux émissions de gaz à effet de serre et donc à la consommation énergétique il ne faut pas mettre de côté les autres impacts prépondérants de l’infrastructure numérique : consommation de métaux et d’autres ressources associées, consommation d’eau, dégradation et pollution des écosystèmes. Pour avoir une image la plus claire possible de l’empreinte environnementale du numérique il est nécessaire d’utiliser tous ces critères et de les étudier avec une méthodologie globalement partagée.

#### La consommation énergétique
Dans un premier temps il s’agit de regarder la consommation énergétique liée à la fabrication et à l’usage des équipements au niveau des centres de données, des réseaux et des équipements utilisateurs. La fabrication d’équipements implique généralement des opérations de minage, de transformation, de transport, d’usinage et d’assemblage. La première question que l’on doit se poser est le type de matériel et d’équipements utilisés à chacune de ces étapes et le type d’énergie qui les fait fonctionner. Un tractopelle ou un camion sur un site minier ont tous deux besoin de carburant pour fonctionner, les génératrices utilisées par les premières opérations de nettoyage du minerai utilisent aussi du fioul, le bateau qui va transporter les minerais jusqu’à l’usine de raffinage va aussi consommer du fioul. L’usine qui va fondre le minerai pour en faire des lingots va être dépendante d’une centrale énergétique alimentée par du charbon, du gaz, du fioul ou d’autres énergies primaires. Une chaîne d’assemblage va aussi utiliser de l’électricité produite à partir de différentes sources d’énergie en fonction des pays. Mais cette consommation énergétique à la fabrication ne se résume pas seulement à l’extraction des minerais, il y a par exemple l’exploitation d’hévéa pour produire le caoutchouc qui enrobe une bonne partie des câbles et la production des composants plastiques par l’industrie pétrochimique, parmi bien d’autres étapes énergivores pour fabriquer cette infrastructure.

{{< aside >}}
#### Connaître les différents types d’énergie
On distingue trois types d’énergie : primaire, finale et utile. L’énergie primaire est l’énergie brute extraite : un baril de pétrole brut, une tonne de charbon, etc. Cette énergie primaire est transformée en énergie finale avec une perte à la transformation de 40% en moyenne. Quand on parle d’énergie finale on parle majoritairement de coke (du charbon passé au four), de gaz purifié, de carburants raffinés (essence, kérosène, etc) et d’électricité. L’énergie utile est la transformation de l’énergie finale en action : utiliser de l’électricité pour allumer un bulbe incandescent, l’explosion de carburant dans un moteur pour produire du mouvement, brûler du gaz purifié pour produire de la chaleur. La transformation de l’énergie finale en énergie utile se fait avec un taux de perte qui dépend de l’usage final. Un bulbe incandescent alimenté à l’électricité a un taux de perte de 95% de l’énergie finale, il n’utilise donc que 3% de l’énergie primaire. Un moteur thermique n’utilise que 20 à 40% de l’énergie contenue dans le carburant, un moteur thermique n’utilise donc que 12 à 24% de l’énergie primaire.
![Les différents types d’énergie](media/schema10@double-page.png)
{{< /aside >}}

Une fois les équipements numériques fabriqués, livrés, vendus et installés, il est nécessaire de les alimenter en énergie pour les faire fonctionner. On utilise un vecteur d’énergie bien connu pour les mettre en route : l’électricité. Nous branchons la grande majorité de nos appareils numériques à une prise et nous les alimentant via un courant électrique. Une télévision connectée, un ordinateur, un frigo connecté, un smartphone, tous ces équipements sont reliés en permanence ou de façon temporaire à une prise. Une batterie permet de stocker de la puissance électrique afin de permettre à un appareil de pouvoir échapper temporairement à un raccordement à une prise électrique. On appelle cela l’autonomie. De plus, un des principaux objectifs d’amélioration de ces appareils est l’efficacité énergétique. Par exemple, on souhaite qu’un serveur informatique, pour 1 kWh (1000 watts consommés en 1h), puisse traiter plus de données. On peut aussi retourner la formulation : pour un 1 Gb de donnés transférées, on souhaite qu’un serveur consomme moins d’électricité. En 2020, les centres de données consomment toujours la même quantité d’électricité qu’en 2015, voire moins, alors que leur charge de travail a été multipliée par 2,7 et que le trafic a été multiplié par 4[^44].

![Les différents pôles et facteurs (sauf électricité) pour définir l’empreinte environnementale du numérique](media/schema11@double-page.png)

Aujourd’hui la plupart des équipements informatiques ont gagné, et continuent encore pour certains, à gagner en efficacité énergétique. Si tous les équipements consomment moins d’électricité alors la consommation électrique du numérique devrait baisser. Pourtant ce n’est pas ce qui est observé aujourd’hui pour plusieurs raisons. L’efficacité énergétique des équipements n’influe que sur une partie de la consommation énergétique totale car elle ne s’applique qu’à l’usage des équipements et à la consommation électrique, pas à la fabrication ni à la consommation d’énergie primaire. De même, des équipements plus efficaces vont être généralement utilisés beaucoup plus. Dans l’exemple donné plus tôt les centres de données consomment le même niveau d’électricité alors que le trafic a été multiplié par 4 entre 2015 et 2020. Si le trafic était le même alors nous aurions pu diviser la consommation électrique des centres de données par 4. Ce phénomène s’appelle le paradoxe de Jevons, ou l’effet rebond, celui-ci définit que lorsque qu’un équipement devient plus efficace, il devient alors moins cher à utiliser, entraînant une hausse de consommation.

{{< aside >}}
#### Rappel sur l’efficacité énergétique
Cet indicateur ne représente que la consommation électrique à l’usage d’un équipement. Il ne donne aucune information sur l’énergie nécessaire à la fabrication de l’équipement, ni sur le type d’énergie utilisée pour produire de l’électricité, ni sur la hausse de la consommation en valeur absolue de l’équipement en question. Si on regarde uniquement la consommation d’électricité d’un équipement on ne regarde alors que 25 à 40% de l’empreinte énergétique totale. L’efficacité énergétique n’est pas un indicateur environnemental à moins qu’il soit couplé avec une politique de baisse de la consommation.
![La consommation d’énergie primaire et sa conversion en énergie finale en France](media/schema12@double-page.png)
{{< /aside >}}

On peut calculer la consommation énergétique d’un équipement durant sa phase de production grâce à une analyse de cycle de vie (ACV). On compte alors l’énergie primaire dépensée, exprimée en joules (J) et parfois convertie en Watt-heure (Wh). On estimait en 2002 qu’une puce électronique de 32 MB consommait 56 MJ durant son cycle de vie dont 27 lié à la fabrication et 15 à l’usage (avec un scénario de 3 heures d’utilisation pendant 4 ans)[^45]. Des études plus récentes sur les semi-conducteurs montrent aussi l’augmentation de la consommation énergétique liée à la fabrication pour produire des composants plus efficaces et performants. Cette augmentation de la consommation d’énergie à la fabrication est notamment liée à des processus industriels plus longs et plus complexes pour produire des composants plus efficaces : un transistor de 350 nanomètres (nm) nécessite 147 étapes de fabrication, un transistor de 45 nm en nécessite 251[^46]. Les impacts liés à la consommation d’eau, la création d’aérosols (​smog​), aux résidus chimiques (acidification), à l’eutrophisation augmentent régulièrement avec la miniaturisation des composants[^47]. Généralement, plus un composant est petit plus il créera d’impacts environnementaux à la fabrication. D’après les mêmes sources la phase d’usage des semi-conducteurs reste la plus consommatrice d’énergie et si on rapporte la consommation énergétique à la puissance de calcul produite (MJ/Millions de transistors), la consommation d’énergie a baissée[^48].


Durant la phase d’usage les méthodes de calcul ont donc tendance à calculer un taux de kWh/GB qui peut être condensée dans la question suivante : combien faut-il d’électricité pour transmettre 1 GB de données ? On appelle cela l’intensité énergétique du transfert de données. De nombreuses études ont tenté d’apporter des éléments de réponse depuis le début des années 2000 mais les données disponibles étaient assez rares et les méthodes divergaient grandement.. Aujourd’hui on commence à comprendre de mieux en mieux combien d’électricité est consommée par l’infrastructure numérique et les méthodes commencent à s’unifier. Le tableau ci-dessous est une méta-analyse[^49] qui répertorie la plupart des méthodes de calcul pour estimer la demande énergétique de la transmission de données sur internet et propose une méthode commune. Comme on peut l’observer les données de référence pour ces méthodes sont plus ou moins vieilles, allant de 2000 à 2015, et seulement quatre études intègrent l’équipement utilisateur final (téléphone, ordinateur, etc).

<table id="tableau4">
    <caption>Récapitulatif des différentes études sur l’intensité électrique de transmission de données sur Internet</caption>
    <thead>
      <tr>
        <th scope="col" class="text-center" width="35%">Étude</th>
        <th scope="col" class="text-center" width="30%">Année de référence des données</th>
        <th scope="col" class="text-center" colspan="2">Frontières du système</th>
      </tr>
      <tr class="text-xxs">
        <th></th>
        <th></th>
        <th scope="col" class="text-center" width="17.5%">Centre de <br>données</th>
        <th scope="col" class="text-center" width="17.5%">Câbles sous-marins</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row" class="text-left">Koomey et al.</th>
        <td class="text-right">2000</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Taylor et Koomey</th>
        <td class="text-right">2000/2006</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Baliga et al.</th>
        <td class="text-right">2008/2008</td>
        <td class="text-right">–</td>
        <td class="text-right">X</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Weber et al.</th>
        <td class="text-right">2008</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Coroama et al.</th>
        <td class="text-right">2009</td>
        <td class="text-right">–</td>
        <td class="text-right">X</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Williams et Tang</th>
        <td class="text-right">2010</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Malmodin et al.</th>
        <td class="text-right">2010</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Malmodin et al.</th>
        <td class="text-right">2010</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Costenaro et Duer</th>
        <td class="text-right">2011</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Shehabi et al.</th>
        <td class="text-right">2011</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Schien et Preist</th>
        <td class="text-right">2011</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Krug et al.</th>
        <td class="text-right">2014</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Schien et al.</th>
        <td class="text-right">2014</td>
        <td class="text-right">–</td>
        <td class="text-right">X</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Malmodin et Lundén</th>
        <td class="text-right">2015</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
      </tr>
    </tbody>
</table>

<table id="tableau4bis">
    <thead>
      <tr>
        <th class="text-center" colspan="4"></th>
        <th scope="col" class="text-center" width="30%">Estimation (kWh/GB)</th>
      </tr>
      <tr class="text-xxs">
        <th scope="col" class="text-center">Cœur du réseau IP</th>
        <th scope="col" class="text-center">Réseaux d'accès</th>
        <th scope="col" class="text-center">Équipement réseau site / domestique</th>
        <th scope="col" class="text-center">Équipement utilisateur</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">136</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">92 à 160 / 9 à 16</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">0,17/0,004 à 0,009</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">7</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">0,2</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">0,3</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">0,08</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">2,48</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">5,12</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">0,29</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">0,02</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">7,2</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">–</td>
        <td class="text-right">0,052</td>
      </tr>
      <tr>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">X</td>
        <td class="text-right">/</td>
      </tr>
    </tbody>
</table>

On retrouve ici un découpage en pôles avec les centres de données, les réseaux et les équipements utilisateurs. Il faut se rappeler que les taux présentés ici sont des moyennes globales qui servent à estimer la consommation électrique de façon globale. Lors de l’analyse d’une action particulière il est toujours plus pertinent d’utiliser des données propres au pays d’usage si elles existent. La communauté française a produit ou a contribué à des rapports de référence sur les impacts environnementaux du numérique. On peut notamment mettre en avant GreenIT, acteur historique depuis 2004, qui a travaillé sur des données issues de leurs propres ACV sur des industriels français et a produit en 2019 l’étude “Empreinte environnementale du numérique mondial”[^50]. GreenIT rappelle à juste titre que l’électricité n’est pas un indicateur d’impact environnementale et a concentré son étude sur la consommation d’énergie primaire (en Joule) plutôt qu’en énergie finale (en Watt-heure)[^51]. En parallèle, une étude publiée en 2015 par Anders Andrae et Tomas Edler sur la consommation électrique des technologies de communication jusqu’en 2030[^52] sert de référence stable pour la publication en 2019 du rapport du think tank français, The Shift Project, “LeanICT : pour une sobriété numérique”[^53]. En 2019, le numérique représenterait 4,2% de la consommation mondiale d’énergie primaire et 5,5% de la consommation électrique mondiale selon GreenIT, soit 24 480 000 Térajoule (TJ) ou 6800 TWh d’énergie primaire, dont 1300 TWh d’électricité consommée[^54]. Le Shift Project propose plusieurs scénarios estimant la consommation énergétique finale du numérique en 2020 entre 2878 (estimation basse) et 5976 TWh (estimation haute). Le Shift Project estime que la consommation énergétique du numérique croît de 9% par an et représenterait 3,3% de la consommation énergétique mondiale. Ces différences de résultat proviennent de l’usage de données de départ différentes et une intégration plus nette de l’énergie primaire à la phase de fabrication dans le rapport de GreenIT. Toutefois les deux rapports convergent vers un ordre de grandeur similaire et surtout vers un taux de croissance annuelle très élevé.

Comprendre la consommation énergétique du numérique revient à regarder l’ensemble de la chaîne de la production, à la fin de vie, en passant par la phase d’usage. Malheureusement, l’énergie consommée par la fin de vie des équipements numériques est toujours mal connue et peu de données sont exploitables aujourd’hui. Il est toujours conseillé de partir de l’énergie primaire pour comprendre les impacts et d’utiliser la consommation électrique avec prudence car celle-ci, associée à l’efficacité énergétique, peut servir à cacher les réels impacts d’un service numérique et des équipements qui y sont liés.

#### Les émissions de gaz à effet de serre
La combustion d’énergies fossiles provoque l’émission de gaz à effet de serre, c’est-à-dire des gaz qui vont retenir une partie du rayonnement solaire à l’intérieur de l’atmosphère et donc une partie de l’énergie solaire. L’accumulation de cette énergie solaire va être absorbée par la surface terrestre et océanique. Ce processus d’échange et de capture de l’énergie solaire dans l’atmosphère fait partie des mécanismes qui ont structuré la vie du Terre. Pendant des centaines de milliers d’années ce processus a suivi un cycle connu et observé oscillant en réchauffement et refroidissement sur des périodes de quelques dizaines de millions d’années. Cependant ce cycle connaît une anomalie pendant un peu moins de deux cents ans. Le réchauffement augmente très rapidement et hors du cycle identifié. Ce réchauffement est dû à la concentration soudaine de gaz à effet de serre dans l’atmosphère à cause des activités humaines industrielles. Plus ces gaz s’accumulent dans l’atmosphère, plus d’énergie solaire est piégée dans l’atmosphère, plus la température augmente en surface des océans et de la terre, entraînant un réchauffement progressif et global. La vitesse actuelle du réchauffement n’a jamais été observé avant par les paléoclimatologues.

Il existe plusieurs types de gaz à effet de serre : la vapeur d’eau (H​<sub>2</sub>O ), le gaz carbonique (C​O<sub>2</sub>​), le méthane (CH​<sub>4</sub>)​, le protoxyde d’azote (N​<sub>2</sub>O​), les perfluorocarbures (CF<sub>4</sub>​)​, les hydrofluoro&shy;carbures (HFC), les hexafluorures de soufre (SF​<sub>6</sub>). Chacun de ces gaz capture plus ou moins bien le rayonnement solaire et chacun a donc un pouvoir de réchauffement relatif. Ce pouvoir de réchauffement dépend aussi du temps de résidence de ces gaz dans l’atmosphère. Pour calculer le pouvoir de réchauffement de ces gaz on utilise l’équivalence carbone, c’est-à-dire qu’on compare leur pouvoir réchauffant par rapport à celui du gaz carbonique sur une durée de 100 ans[^55].

<table id="tableau5">
    <caption>Récapitulatif des différents gaz à effet de serre et leur potentiel de réchauffement (GIEC)</caption>
    <thead>
      <tr>
        <th scope="col" class="text-center" width="30%">Gaz</th>
        <th scope="col" class="text-center">Formule</th>
        <th scope="col" class="text-center text-xxs">Pouvoir de réchauffement <br>(comparé à CO<sub>2</sub>/100 ans)</th>
        <th scope="col" class="text-center">Pourcen&shy;tage</th>
        <th scope="col" class="text-center">Équivalent <br>carbone</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row" class="text-left">Vapeur d’eau</th>
        <td class="text-right">H<sub>2</sub>O</td>
        <td class="text-right">–</td>
        <td class="text-right">55%</td>
        <td class="text-right">–</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Gaz carbonique</th>
        <td class="text-right">CO<sub>2</sub></td>
        <td class="text-right">1</td>
        <td class="text-right">39%</td>
        <td class="text-right">0,273</td>
      <tr>
        <th scope="row" class="text-left">Méthane</th>
        <td class="text-right">CH<sub>4</sub></td>
        <td class="text-right">25</td>
        <td class="text-right">2%</td>
        <td class="text-right">6,82</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Protoxyde d'azote</th>
        <td class="text-right">N<sub>2</sub>O</td>
        <td class="text-right">298</td>
        <td class="text-right">1%</td>
        <td class="text-right">81,3</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Perfluorocarbures</th>
        <td class="text-right">C<sub>n</sub>F<sub>2n+2</sub></td>
        <td class="text-right">7400 à 12200</td>
        <td class="text-right">faible</td>
        <td class="text-right">2015 à 3330</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Hydrofluoro&shy;carbures</th>
        <td class="text-right">C<sub>n</sub>H<sub>m</sub>F<sub>p</sub></td>
        <td class="text-right">120 à 14800</td>
        <td class="text-right">faible</td>
        <td class="text-right">34 à 4040</td>
      </tr>
      <tr>
        <th scope="row" class="text-left">Hexafluorore de soufre</th>
        <td class="text-right">SF<sub>6</sub></td>
        <td class="text-right">22800</td>
        <td class="text-right">faible</td>
        <td class="text-right">6220</td>
      </tr>
    </tbody>
</table>

Un kilo de méthane a un pouvoir réchauffant 25 fois supérieur à 1 kilo de gaz carbonique sur 100 ans. Pour définir l’équivalent carbone (EC), la formule est légèrement différente. Le gaz carbonique est composé d’oxygène (O2) et de carbone (C), il faut donc seulement prendre en compte le poids du carbone dans un kilo de gaz carbonique pour définir l’équivalent carbone. Dans un kilo de gaz carbonique le carbone représente 273 grammes. C’est pour cela qu’on définit l’équivalent carbone du gaz carbonique à 0,273. Tous les autres gaz à effet de serre auront une équivalence carbone calculée à partir de cette donnée. Cependant tous ces gaz à effet de serre ne sont pas émis en quantité équivalente, le gaz carbonique représente aujourd’hui 39% des gaz à effet de serre émis, c’est pour cette raison que l’on calcule l’effet réchauffant des autres gaz à partir du carbone. La vapeur d’eau a un cycle très rapide et ne reste pas assez longtemps dans l’atmosphère pour être intégrée dans ce calcul.

Cette très forte présence du carbone dans l’atmosphère est dûe à la combustion d’énergie à base de carbone (charbon, gaz, pétrole). Ces énergies ont structuré le développement des sociétés industrielles et les infrastructures globales repose majoritairement sur celles-ci. Comme expliqué précédemment, ces énergies sont consommées autant à la fabrication qu’à l’usage des équipements informatiques (centres de données, réseaux, équipements utilisateurs). L’infrastructure numérique est donc émettrice de gaz à effet de serre. Le rapport de GreenIT estimait que le numérique était responsable de 3,8% des émissions mondiales de gaz à effet de serre en 2019[^56]. De son côté le Shift Project estimait qu’en 2020 le numérique représenterait 4% des émissions mondiales de gaz à effet de serre avec un taux de croissance annuelle de 8%. En suivant ce taux de croissance les émissions du numérique pourraient représenter 7,5% des émissions mondiales en 2025[^57]. GreenIT offre une vision plus détaillée de la source de ces émissions en estimant 63-66% des émissions du numérique proviennent des équipements utilisateurs (40% pour la fabrication, 23-26% pour l’usage), 19-22% pour les réseaux, 15% pour les centres de données[^58].

En valeur absolue le numérique n’est pas le secteur d’activités qui émet le plus de gaz à effet de serre, la production d’énergie, le transport et l’agriculture restent loin devant. Cependant le numérique est le secteur d’activité avec le plus gros taux de croissance. Plus ce secteur va augmenter sa consommation d’énergie en produisant des nouveaux équipements et en augmentant la production d’électricité plus ces émissions vont augmenter. D’après les accords de Paris, les émissions de gaz à effet de serre devraient réduire de 5% par an et chaque secteur doit faire sa part. Avec un taux de croissance annuel de 8%, le secteur numérique pourrait alors avoir un retard de 13%, allant alors à l’encontre de toutes les stratégies de transition.

#### La consommation de métaux
L’infrastructure numérique consomme de nombreux métaux afin de produire des équipements informatiques. L’extraction et le traitement de ces métaux vont demander de l’énergie et de l’eau avant d’être transportés et vendus sur les marchés de matières premières. Quels sont alors les impacts liés à ces activités et quels poids pèsent t-ils et à quelle vitesse évoluent-ils ? De plus, les métaux ne sont pas des ressources renouvelables et l’infrastructure numérique telle que nous la connaissons n’existerait pas sans l’extraction croissante de métaux, alors quelles sont les réserves et comment les calcule t-on ?

L’extraction de métaux requiert l’ouverture d’une mine, soit souterraine, soit ouverte. On ouvre des mines car l’industrie requiert plus de matières premières. En effet, les investisseurs ne financent généralement pas une opération de minage tant que la demande et la rentabilité est possible. C’est donc la demande qui pilote la production de matières premières minérales : l’offre s’adapte à la demande. Or, l’industrialisation croissante a fortement augmenté la demande. Les sociétés humaines, surtout industrielles, consommaient 210 mégatonnes (Mt) de minerais dans le monde en 1900, puis 6,5 gigatonnes (Gt), soit 6500 mégatonnes, en 2009[^59]. Durant cette période d’un peu plus d’un siècle la population a été multipliée par 4 tandis que la consommation de minerais et de minéraux industriels a été multipliée par 30[^60]. Celui est majoritairement dû à des sociétés et à des classes sociales bien précises. À ce titre, la multiplication par 30 de la consommation de minerais n’est pas imputable à l’ensemble de l’humanité mais bien à des sociétés sur certains territoires durant une certaine période de temps.

Au moment où la demande émerge du secteur industriel et financier l’offre ne s’aligne pas instantanément comme s’il s’agissait de libérer des stocks d’un hangar. Il faut compter plusieurs décennies entre le début de l’exploration géologique d’un sol, les premiers forages, l’ouverture de la mine, la mise en exploitation et l’extraction de la première tonne de minerai. Dans le cas d’une exploration dans une zone déjà connue ce délai de déploiement peut être assez “rapide”, entre 10 et 15 ans. Dans le cas d’une exploration dans une zone peu connue il faut alors plutôt compter entre 20 et 30 ans. Il faut alors bien comprendre que si une demande émerge sur un métal en 2020 et que des lignes d’investissement se débloquent pour de l’exploration géologique en même temps, alors les premières tonnes extraites (l’offre) pour répondre à cette demande seront disponibles entre 2030 et 2050. Par exemple, Alain Geldron, l’un des experts nationaux en matières premières, rappelle que la mine d’Oyu Tolgoi, l’un des plus gros gisements de cuivre au monde, a été mise en exploitation au sud de la Mongolie en 2013 : “l’exploration a débuté dans les années 80, avec les premiers forages en 1997. La décision de mise en exploitation intervient en 2005 et la première tonne de minerai a été sortie en 2013. Le gisement sera en pleine production en 2021 avec 400 000 t/an de cuivre contenu extrait. Soit plus de 35 ans au total d’exploration et de mise en exploitation.”[^61] Si la demande augmente brutalement alors les réserves diminuent tant que de nouvelles mines ne sont pas ouvertes. On n’observe toutefois pas de pénurie immédiate car la baisse des réserves, comme l’augmentation des capacités de production, est généralement progressive.

Il s’agit maintenant de bien comprendre comment on qualifie l’importance des ressources minérales et des réserves exploitables. On distingue bien deux termes : ressources d’un côté, réserves de l’autre. Les “ressources”, en géologie, correspondent à la concentration d’une substance liquide, solide ou gazeuse dans la croûte terrestre et dont l’extraction économique paraît faisable ou potentielle. Ainsi la définition des ressources varie grandement en fonction des connaissances géologiques et des outils de mesure à disposition. Plus les techniques d’exploration et d’extraction se perfectionnent plus les ressources découvertes ou extractibles peuvent augmenter. Plus on est certain d’une concentration de minerai dans les sols plus la ressource est économiquement qualifiée. On parle alors de ressources “supposées” pour les ressources les moins bien identifiées, de ressources “indiquées” et de ressources “mesurées” pour celles qui sont mieux connues. Il y a aussi des ressources “non identifiées” qui sont une estimation établie à partir de probabilités mathématiques renseignées par les explorations précédentes.

![Identification des réserves et des ressources minérales](media/schema13@double-page.png)

Les réserves correspondent aux concentrations de minerai connues et exploitables économiquement et techniquement à un prix donné à un moment donné. C’est donc à la fois les prix du marché, les progrès technologiques dans les méthodes d’extraction et les coûts d’extraction et de traitement, la concentration géologique, la qualité et la profondeur des filons qui indiquent quelle part des ressources mesurées deviennent des réserves exploitables. Ils existent plusieurs types de réserves, deux sont prépondérantes : l’assiette de réserves (​reserves base​ en anglais) englobe l’ensemble des ressources prouvées (mesurées et indiquées) qui sont exploitables par les pratiques minières actuelles. L’assiette de réserves inclut les réserves qui sont économiquement exploitables au moment de l’évaluation (réserves économiques), celles presque exploitables économiquement (réserves marginales), et celles qui ne sont pas du tout exploitables économiquement pour l’instant (réserves sub-économiques). C’est à partir de l’assiette de réserves que la réserve mondiale d’une ressource est calculée. Ce qu’on appelle les “réserves” correspond à la part de l’assiette de réserves qui est exploitable en termes technique, géologique et économique au moment de l’évaluation. Cela ne veut pas dire que toutes les réserves sont exploitées mais plutôt qu’elles pourraient l’être à l’instant ​t​.

La définition des ressources et des réserves est donc un exercice complexe qui dépend de facteurs géologiques (localisation, concentration, qualité, profondeur du gisement), de facteurs scientifiques et technologiques (état des connaissances d’exploration, de forage, de production, conception des machines nécessaires), et de facteurs économiques (prix sur les marchés de matière première, coût de l’extraction, coût de l’énergie nécessaire à l’extraction, coût d’investissement et d’exploitation). D’autres facteurs entrent aussi en jeu comme le facteur géopolitique : les ressources ne sont pas réparties équitablement dans la croûte terrestre, certains pays possèdent des sols plus “riches” que d’autres. Il y a aussi les facteurs sociaux et environnementaux, l’ouverture d’une mine et le coût d’extraction dépend aussi des normes sociales (protection du personnel, politique salariale, etc.) et environnementales (normes de pollution et de dépollution, usage de l’eau, etc.) du pays où se situe l’exploitation. La criticité d’une ressource, plutôt que son épuisement, est donc relative à l’ensemble de ces critères.

Lorsqu’on essaye de calculer l’épuisement d’une réserve et le moment où son exploitation s’arrêterait on met en relation les réserves connues, c’est-à-dire l’assiette de réserves (R) à l’instant ​t ​et la production minière (P) au même instant. Ce rapport de R sur P (R/P) s’appelle le taux d’épuisement/criticité (le terme exact est : ​burn rate​) et s’exprime en années. Si mes réserves à l’instant donné sont de 10 et que ma production minière au même moment est de 1 chaque année, alors j’ai 10 ans de réserves avant épuisement (10/1). Ce rapport R/P n’est cependant pas une date limite inflexible, il existe de nombreux facteurs qui font varier cette date. Tout d’abord les découvertes géologiques, l’amélioration des techniques d’exploitation, et une augmentation du prix de la ressource[^62] peuvent faire passer des ressources supposées dans la catégorie de l’assiette de réserves, augmentant les réserves connues et repoussant la limite. Alain Geldron rappelle “qu’en 1950 les réserves de cuivre étaient d’un peu moins de 100 Mt elles sont actuellement de 720 Mt, alors que dans le même temps la production, tirée par la demande, est passée de 2,4 Mt/an à 18,7 Mt/an. Ainsi la durée de vie des réserves connues de cuivre reste depuis 60 ans autour de 40 années”[^63]. Un autre facteur prépondérant est à souligner : l’augmentation exponentielle de la consommation des métaux. Plus la croissance continue plus nous consommons vite nos réserves : si nous consommons 100 chaque année et que le taux de croissance annuelle de la consommation est de 3%, alors, 23 ans plus tard, nous consommerons 200 chaque année. En fait, Alain Geldron rappelle que notre consommation de métaux double tous les 25 ans et consomment plus rapidement nos réserves : “en appliquant un taux de croissance annuel de 3% au cuivre (celui observé depuis 100 ans) la disparition des réserves actuelles ne serait plus dans 40 ans mais dans 26 ans avec une consommation annuelle de 39 Mt soit à peu près le double de la consommation actuelle”[^64].

![Évolution des réserves, de la production et du ratio R/P pour certaines matières minérales à consommation constante 2016 et à consommation évoluant avec le taux de croissance mesuré entre 2000 et 2016](media/schema14@double-page.png)

Il est donc difficile de prévoir l’épuisement physique des ressources car le calcul des réserves dépend de nombreux facteurs relatifs et changeant d’années en années. Toutefois le taux de croissance actuelle de la consommation de métaux impose une course que les capacités de production pourront difficilement suivre pour toutes les raisons évoquées plus hauts. Une fois de plus, Alain Geldron avertit que “tout métal dont le rapport R/P est inférieur à 20 ans, s’il n’est pas substituable aisément, ce qui est le cas pour la plupart, […] il est susceptible de voir se produire des pénuries conjoncturelles plus ou moins longues. Ainsi la compagnie minière Rio Tinto, se basant sur des travaux de consultants, envisage une pénurie de production de cuivre peu après 2020”[^65]. Il faut bien alors comprendre que l’indicateur d’épuisement d’une ressource dans la croûte terrestre ne correspond à la disparition effective de la ressource à l’échelle temporelle d’une vie humaine, mais correspond plutôt à un avertissement sur la fragilité de nos réserves par rapport à notre consommation actuelle et future de ressources[^66]. Sur un échelle à court et moyen terme il est donc pertinent d’utiliser le terme de criticité des ressources plutôt que d’épuisement.

{{< aside >}}
#### Extraire la Terre
Le chercheur Florian Fizaine propose une expérience de pensée intéressante pour réfléchir à l’idée d’épuisement des ressources : à supposer que l’ensemble de la masse de la Terre soit exploitable (soit environ 6 x 102​ 4​ kilos) et que la consommation mondiale continue de croître à un taux de 3 % par an en partant d’une consommation annuelle mondiale en ressources (toutes ressources confondues) évaluée à 90 milliards de tonnes (dont 70 de ressources minérales), il faudrait à l’humanité environ 850 ans pour engloutir la Terre[^67].

![La masse de la Terre et l’épaisseur de sa lithosphère exploitable](media/schema15.png)

Sachant que nous n’irons évidemment pas exploiter les ressources terrestres au-delà d’une certaine profondeur se limitant a priori à la lithosphère, l’échéance réelle sera bien plus proche. À titre d’illustration : la mine d’or de Mponeng, en Afrique du Sud, la plus profonde du monde, n’atteint que quelque 4 kilomètres de profondeur. De plus, Alain Geldron rappelle que “les régions et les pays ne sont pas dotées de gisements de façon égale, certains en sont même dépourvus. Par exemple il ne peut y avoir de gisements métalliques dans le bassin parisien sauf à envisager d’aller explorer sous cette formation géologique au-delà de 3000 mètres pour ce qui est de son centre, ce qui n’est actuellement pas envisageable”[^68].
{{< /aside >}}<span hidden>[^67]</span><span hidden>[^68]</span>

Mais alors pourquoi notre consommation de matières premières et de métaux croît autant ? Qu’en est-il du numérique ? De façon globale la consommation a augmenté et croit à cause du développement industriel, c’est-à-dire que l’on utilise de plus en plus de métaux dans la production industrielle. Dans le contexte de l’augmentation de leur niveau de vie, les foyers, augmentant leurs revenus, consomment de plus en plus de produits contenant des métaux (augmentation de la demande individuelle en métaux)[^69]. Finalement la consommation augmente car la population augmente mais il ne semble pas que cela soit le facteur principale puisque, comme vu précédemment, entre 1900 et 2009 la population a augmenté par 4 mais la consommation de métaux a été multiplié par 30.

Dans le secteur numérique on distingue deux catégories de métaux, ceux utilisés en grande quantité pour leurs fonctions structurelles (réseaux de télécommunication → cuivre, aluminium, certains aciers) et les métaux utilisés en faible quantité pour leurs propriétés exceptionnelles dans l’industrie high-tech​ (petits métaux et métaux précieux)[^70]. Au total, le secteur numérique sollicite plus de 60 métaux dans des volumes très diverses et plus de la moitié de ces métaux dépendent de l’extraction d’autres métaux. En effet, certains petits métaux demandés par le secteur numérique ne représentent pas un volume de demande suffisamment important pour justifier l’ouverture d’une mine dédiée, alors on les extrait dans des mines dont la rentabilité dépend de l’extraction massive de grands métaux. Par exemple, le gallium dépend de l’extraction de la bauxite d’aluminium (liée à la production d’aluminium) et le germanium n’est récupérable qu’à partir de minerai de zinc[^71]. Cette interdépendance peut poser problème car cela implique que l’augmentation de la demande sur un petit métal ne va pas entraîner une augmentation de l’offre car il faudrait augmenter la production du grand métal dont le petit métal dépend. Si la demande pour le germanium augmente alors il faut augmenter la production de zinc, pour justifier cette augmentation de la production il faut alors qu’elle soit économiquement intéressante et donc que la demande de zinc augmente. En somme, l’offre des petits métaux et des métaux précieux ne s'adaptera pas toujours à la demande à cause de leur co-dépendance.

![Principales interdépendances entre grands et petits métaux](media/schema16@double-page.png)

Quelles sont alors les différences de production entre les grands métaux et les petits métaux et métaux précieux ? Pour ce qui est des grands métaux comme le cuivre, l’aluminium, le fer ou l’étain par exemple, leur consommation augmente aussi. D’après l’International Copper Study Group, la consommation totale de cuivre dans le monde s’élevait à 24 millions de tonnes en 2017. La part de consommation liée à l’usage du cuivre comme conducteur électrique a atteint quasiment 60 %, “soit environ 11 millions de tonnes pour la génération, la distribution et la transmission d’électricité, et 3 millions de tonnes pour le sous-ensemble des équipements électriques et électroniques auquel le numérique appartient” comme le rappelle Liliane Dedryver pour France Stratégie[^72]. La demande en métaux du numérique rentre donc en compétitivité avec d’autres usages, notamment le secteur énergétique qui en immobilise de grandes quantités. En effet, bien que le cuivre soit très bien recyclable et recyclé celui-ci est immobilisé dans de nombreuses structures (bâtiment, énergie, etc.) et ne peut pas être récupéré, provoquant encore une augmentation de la demande. En 2017, Réseau de transport d’électricité (RTE) estimait que les réseaux électriques français représentaient une immobilisation de 170 000 tonnes de cuivre. Cette quantité devrait augmenter de 30 000 tonnes d’ici 2026 pour assurer le raccordement du parc éolien en mer et les interconnexions aux frontières[^73]. Au niveau de l’infrastructure numérique ce sont les réseaux de télécommunications qui utilisent et immobilisent le plus de cuivre. Le réseau cuivre d’Orange comporte par exemple 110 millions de paires-kilomètres de câbles de cuivre[^74]. En comparaison avec les réseaux de télécommunications, la demande en cuivre pour la fabrication des équipements électroniques est relativement plus faible. France Stratégie estime “qu’une tonne de cuivre est nécessaire pour produire 250 000 smartphones et tablettes, les 1,4 milliard de nouveaux smartphones vendus dans le monde en 2017 (dont 20 millions en France) ont nécessité la consommation de 5 600 tonnes de cuivre (dont 80 tonnes pour la France)”[^75].

Du côté des petits métaux les volumes extraits sont largement différents. Par exemple la production annuelle de tantale était estimé à 1 800 tonnes en 2017 et en 2018[^76]. Au moins la moitié de la production de ce métal a utilisé pour la fabrication de condensateurs électroniques. Ces condensateurs ont notamment permis la miniaturisation des équipements numériques. La fabrication de l’électronique grand public tire majoritairement à la hausse la production de ce métal et entraîne un taux de croissance annuel estimé à 5,8%[^77]. D’autres métaux comme le gallium et le germanium sont utilisés pour leurs propriétés dans la fabrication des semi-conducteurs. Leur production annuelle est respectivement estimée à 320 et 106 tonnes. Moins d’un gramme de chacun de ces métaux sont utilisés dans la fabrication d’un smartphone. Bien que le volume produit soit faible, les équipements numériques représentent la majeure partie de la demande.

L’extraction de ces métaux représentent une consommation d’énergie et d’eau importante. Étant donné que la question de l’eau sera traitée dans la section suivante nous allons regarder uniquement du côté de l’énergie. En 2010, 10 % de l’énergie primaire mondiale (dont 6,5 % pour l’acier et 0,3% pour le cuivre[^78]) était consacrée à extraire, transporter et raffiner les ressources métalliques tous secteurs confondus[^79]. De plus, d’après une étude menée en 2011 la consommation d’énergie primaire liée à l’extraction de métaux pourrait augmenter de 40% d’ici 2030 à cause de l’augmentation de la consommation et de la baisse de concentration des métaux dans les minerais extraits[^80]. En effet, les gisements les plus concentrés en métaux ont été les premiers à être exploités et au fur et à mesure que nous continuons d’exploiter de nouveaux gisements, ceux-ci ont une concentration en métaux de plus en plus basse et le coût en énergie augmente de plus en plus. S’appuyant sur les travaux de l’économiste Florian Fizaine, Alain Geldron explique que “ si la consommation finale d’énergie a été multipliée par deux entre le milieu du siècle dernier et 2011 alors la consommation énergétique de l’exploitation minière et des carrières a été multiplié par 4 dans le même temps”[^81]. Celui-ci rappelle que “la consommation énergétique des exploitations minières constitue, avec les coûts d’exploration et de mise en exploitation, un élément clé du prix des matières premières minérales dans le futur”[^82]. France Stratégie abonde dans le même sens : “s’il fallait extraire 55 tonnes de minerai pour produire une tonne de cuivre dans les années 1930, il en faut aujourd’hui 125 pour le même résultat”[^83]. La taille du grain voulu, c’est-à-dire la méthode broyage du minerai, ainsi que les méthodes d’excavation influe aussi grandement sur la consommation énergétique des métaux.

![Consommations énergétiques unitaires moyennes des différents métaux (en GJ/t)](media/schema17@double-page.png)

Que retenir de la question des métaux ? Nous pourrions théoriquement continuer à repousser les limites des réserves connues et ne pas connaître d’épuisement des métaux si tant que nous ayions à disposition des connaissances scientifiques et du matériel de production adéquats, un prix de la ressource attractif, des coûts d’exploitation (et donc un coût de l’énergie) suffisamment bas. Dans ces conditions nous pourrions aller exploiter des gisements difficiles d’accès, comme les gisements non-conventionnels et les gisements sous-marins profonds. Toutefois, ces conditions vont être de plus en plus difficiles à réunir, malgré une augmentation de la demande les meilleurs gisements ont déjà été exploité et les rendements des gisements baissent. L’ingénieur Philippe Bihouix décrit le cercle vicieux suivant : les minerais sont de moins en moins concentrés, il faut donc toujours plus d’énergie pour les extraire, les équipements énergétiques utilisent de plus en plus de métaux, il est donc toujours plus difficile de produire de l’énergie et d’extraire de nouveaux métaux. On peut supposer que nous aurons un un problème de criticité du à un problème d’énergie, un problème d’eau, un problème social et un problème environnemental avant d’avoir un problème de disponibilité des ressources. Reste à savoir pendant combien de temps nous sommes capables de payer ce prix et si nous souhaitons réellement payer ce prix. Alain Geldron estime que “l’épuisement des ressources minérales avant la fin de ce siècle est très peu probable pour la plupart des matières premières minérales par contre des pénuries sur des durées importantes sont à envisager, faute d’une mise en exploitation suffisamment rapide de nouveaux gisements”[^84].

Exercice : Exercice : à partir des informations données dans cette partie calculer combien de cuivre est utilisé en moyenne pour la fabrication d’un smartphone et calculer la consommation d’énergie primaire liée à l’extraction du cuivre pour un smartphone.

#### L’empreinte en eau
Les systèmes numériques utilisent de vastes quantités d’eau, pour l’extraction des minerais, pour le refroidissement des installations ou encore pour la production d’électricité alimentant l’ensemble de l’infrastructure, du centre aux données aux équipements utilisateurs. On pourrait distinguer deux types d’empreinte hydrique, la consommation nette d’eau, c’est-à-dire, l’eau consommée directement par l’infrastructure, et l’utilisation de flux d’eau, c’est-à-dire, l’eau prélevée et ensuite reversée, comme pour le refroidissement d’un centre de données.

Aujourd’hui la plus grande partie de l’empreinte en eau du numérique concerne les opérations de minage. L’eau est nécessaire dans presque chaque étape de la production de métaux. Le broyage du minerai extrait, afin d’obtenir un grain plus ou moins épais, ainsi la phase de concentration du minerai représentent 70% de la consommation en eau de l’industrie minière[^85]. Le reste de la consommation d’eau dans un site minier correspond alors à l’approvisionnement en eau du personnel et au transport des résidus (boues, poussières, etc). On estime que pour une puce électronique de 1cm2​ ​, 18 à 27 litres d’eau sont consommés. Cela revient à dire que pour 1 kilogramme de puces électroniques il faudrait alors consommer 10 000 litres d’eau[^86]. À titre de comparaison, 15 500 litres d’eau sont nécessaires pour produire 1 kilogramme de boeuf, 3 400 litres d’eau pour 1 kilogramme de riz et 460 litres d’eau pour 1 kilogramme d’oranges[^87]. À une échelle plus grande, une usine de fabrication de semiconducteurs standard consommerait entre 7 500 000 et 15 000 000 millions de litres d’eau purifiée par jour[^88]. En 2015, Intel déclarait consommer 37 millions de mètres cube (Mm3​ ​) d’eau douce par an pour la fabrication de ses semi-conducteurs, soit 14 800 piscines olympiques[^89]. Samsung estimait pour sa part que sa consommation d’eau pour la fabrication de semi-conducteurs était de l’ordre de 90 Mm3​ ​ en 2015, soit 36 000 piscines olympiques.

![Comparaison de la consommation de litres d’eau pour un kilo de produit](media/schema18.png)

{{< aside >}}
#### Usage extractif de l’eau et impacts régionaux de l’extraction
* Utilisation directe :
Suppression des poussières
Traitement des minéraux
Approvisionnement pour les salariés
Transport des minerais et des résidus

* Utilisation indirecte :
Évaporation des réservoirs
Acheminement vers des communautés voisines 
Eau utilisée pour l’environnement alentour

* Impacts régionaux potentiels :
Impact sur la taille et la qualité des cours d’eau
Épuisement des eaux souterraines
Détournement de cours d’eau

<small>source :  Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 24.</small>
{{< /aside >}}

Après leur fabrication, l’usage des équipements utilisateurs nécessite la production d’électricité qui provient de différentes sources d’énergie primaire. Par exemple, on estime en France que 4 litres d’eau douce sont nécessaires à la production d’un kWh[^90], souvent lié à l’évaporation de l’eau dans les centrales nucléaires. Cette eau est considérée comme perdue comme il faudra attendre qu’elle fasse son cycle, le fameux cycle de l’eau, avant d’être de nouveau “disponible”. On estime en France que l’empreinte eau du numérique, c’est-à-dire avec l’empreinte eau des produits importés, est de 559 millions de m3​ ​ d’eau douce, soit 10,2% de la consommation française[^91]. À l’échelle mondiale, on estime que 7,8 milliards de m3​ ​ d’eau douce sont consommés par l’infrastructure numérique[^92] en 2018, soit 0,2% de l’eau douce disponible au niveau mondiale. En 2014, la consommation mondiale d’eau douce était de l’ordre de 4 000 milliards de mètres cube[^93].

Au delà de la consommation directe, l’infrastructure numérique prélève de l’eau notamment pour refroidir les centres informatiques, avant de la reverser à une température légèrement supérieure. Les réserves de flux d’eau disponible chaque année dépend largement des conditions géologiques, géographiques et climatiques de chaque territoire. Ainsi un flux d’eau dans un territoire donné n’est pas extensible, si le flux d’eau est dirigé vers un nouvel usage c’est qu’il ne va pas autre part. Le prélèvement d’eau de l’infrastructure numérique rentre donc en concurrence avec d’autres secteurs, comme l’agriculture par exemple. Les excès de prélèvement peuvent être compensés par les réserves souterraines pendant un temps, si tant est que l’on laisse ces réserves se régénérer. En France, la consommation d’eau des centres de données pour leur refroidissement n’est pas problématique aujourd’hui car la plupart des territoires français possèdent des réserves hydrologiques importantes (sauf certains bassins connus comme l’Allier, la Creuse, etc.). Toutefois, dans des territoires où l’eau est une ressource précieuse les prélèvements du numérique peuvent devenir problématique. En Californie, un centre de données de 15 MW peut utiliser jusqu’à 1 600 000 litres d’eau par jour (1 600m3​ ​) pour son refroidissement[^94]. Les collectivités doivent alors mettre à jour leur infrastructure de distribution d’eau pour répondre aux besoins de ces installations. Des nouvelles pratiques émergent pour réduire cette consommation (utilisation de circuit fermé, d’eaux grises) mais elles restent encore assez marginales.

Ces prélèvements sont d’autant plus problématiques dans la phase de fabrication car c’est là que se concentre l’utilisation de l’eau, à hauteur de 79% d’eau utilisée par la fabrication et 21% pour l’utilisation[^95]. La consommation en eau des mines entre en concurrence avec d’autres secteurs et d’autres usages dans les territoires de leur implantation. Le Columbia Center on Sustainable Investment estime qu’environ “70% des exploitations minières des six principales compagnies minières dans le monde sont localisées dans des pays où il existe un stress hydrique”[^96]. Certaines exploitations minières situées dans des pays en stress hydrique acheminent parfois de l’eau salée directement depuis la côte. Par exemple, la compagnie BHP Billiton a construit une installation sur la côte chilienne pour acheminer de l’eau pour ses sites miniers à plus de 150 km à l’intérieur des terres[^97]. Toutefois, la désalinisation de l’eau n’est pas une solution à moyen et long-terme car le processus de désalinisation consomme énormément d’énergie : pour 4% d’eau désalinisée dans l’approvisionnement global il faudrait 60% de la consommation énergétique du secteur[^98].

De nombreux progrès ont été fait pour réduire la consommation d’eau du numérique, autant à la fabrication qu’à l’utilisation. Toutefois, il faut extraire toujours plus de minerai pour obtenir la même quantité de métal donc il faudra utiliser toujours plus d’eau. La consommation d’eau du numérique rentre en concurrence avec de nombreux usages dans un territoire donnée et avec les communautés qui l’habite : agriculture, accès à l’eau potable, hygiène, usages quotidiens. Cette concurrence exacerbe la fragilité de territoires déjà en stress hydrique et qui vont être de plus en plus impactés par le changement climatique. À terme, des arbitrages quotidiens pourraient être nécessaires pour décider de l’allocation des flux d’eau. Il faudra alors être particulièrement vigilant pour que l’infrastructure numérique ne concurrence pas les besoins essentiels des communautés qui sont généralement bien loin des lieux d’usage du numérique et parfois invisibles auprès des utilisateurs de cette infrastructure.

#### Fin de vie, dégradations sanitaires et environnementales
De par les connaissances et les données que nous avons actuellement sur l’infrastructure numérique l’emphase est plutôt mise sur la fabrication et l’usage mais rarement sur la fin de vie des équipements numériques. De plus, les impacts environnementaux liés aux gaz à effet de serre, à la consommation d’énergie, d’eau et de métaux sont mieux connus que ceux liés des substances chimiques et plus simplement par l’occupation et la dégradation des écosystèmes. Bien que relativement peu de données soient accessibles il est nécessaire d’aborder ces autres impacts environnementaux et sanitaires pour bien mesurer l’ampleur de ce qu’il reste à explorer concernant l’empreinte de l’infrastructure numérique. Dans un premier temps nous allons regarder de plus près la fin de vie des équipements puis nous nous intéressons plus précisément aux différents types de pollutions tout au long du cycle de vie d’un équipement.

La fin de vie d’un équipement implique généralement qu’il soit transformé en “déchet”, c’est-à-dire qu’il n’est plus utilisable ou valorisable selon les standards de l’équipementier et du pays qui produit le “déchet”, cela ne veut pas dire que l’équipement n’est plus utilisable, réparable, valorisable en soi. Les équipements numériques rentrent dans la catégorie des déchets d’équipements électriques et électroniques, communément appelés les D.E.E.E. Cette catégorie inclut les équipements d’échange thermique (frigo, congélateur, climatiseur), les écrans (T.V., ordinateurs portables, tablettes), les lampes, les gros équipements (lave-linge, sèche-linge, lave-vaisselle, etc.), les petits équipements (aspirateur, four micro-onde,grille-pain, etc.), et les petits équipements informatiques et de télécommunications (téléphone portable, GPS, calculette, imprimante, téléphone, routeur, smartphone). Les déchets des équipements numériques se situent donc au niveau des écrans et des petits équipements informatiques et de télécommunications[^99]. En 2019, 53 Mt[^100] de D.E.E.E. ont été générés dans le monde, soit 7,3 kg par habitant. En 2014, la production annuelle de D.E.E.E. était de 44,4 Mt, soit une croissance annuelle de 3,8%[^101] entre 2014 et 2019. Ce taux de croissance est le plus élevé de toutes les filières de déchets. Cette hausse de la production de déchets peut être expliquée par une augmentation de la consommation des équipements électriques et électroniques, des durées de vie plus courtes et une très faible offre de réparation.

Sur les 53 Mt de D.E.E.E. générés dans le monde en 2019, 6,7 Mt provenaient des écrans, tablettes, ordinateurs portables et téléviseurs, et 4,7 Mt provenaient des petits équipements informatiques et de télécommunications. Entre 2014 et 2019, la génération de déchets dans ces deux catégories s’est stabilisée : la production annuelle de déchets d’écrans, tablettes, ordinateurs et téléviseurs a baissé de 1% par rapport à 2014, celle des petits équipements informatiques a augmenté de 2% pour la même période[^102]. Cependant ces valeurs n’évoluent pas de la même façon entre les différentes régions du globe. L’Europe produit le plus grand poids de e-déchets par habitant, très loin devant l’Afrique et l’Asie bien que cette dernière produise un gros volume de déchets, notamment due à sa population plus importante. Cependant, l’Europe est la région qui a le plus gros taux de collecte des e-déchets grâce à des politiques nationales et européennes relativement efficaces. À l’échelle mondiale sur 53,6 Mt de e-déchets, 44,3 Mt ne sont pas documentés et finissent généralement enterrés, échangés ou recyclés hors des normes environnementales existantes (dans les pays européens, 0,6 Mt de e-déchets finissent directement les poubelles)[^103]. Pourtant la valeur en matières premières récupérables est très élevée, on estime que les métaux contenus dans ces déchets (aluminum, cuivre, fer, etc.) ont une valeur de 57 milliards de dollars US (pour 53,6 Mt)[^104].

<table id="tableau6">
    <caption>Tableau Production et collecte des e-déchets par continent en 2019</caption>
    <thead>
      <tr>
        <th scope="col" class="text-center">Indicateur</th>
        <th scope="col" class="text-center">Afrique</th>
        <th scope="col" class="text-center">Amériques</th>
        <th scope="col" class="text-center">Asie</th>
        <th scope="col" class="text-center">Europe</th>
        <th scope="col" class="text-center">Océanie</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td scope="row">Population dans la région (en millions)</td>
        <td class="text-right">1174</td>
        <td class="text-right">977</td>
        <td class="text-right">4364</td>
        <td class="text-right">738</td>
        <td class="text-right">39</td>
      </tr>
      <tr>
        <td scope="row">Poids (kg/hab)</td>
        <td class="text-right">2,5</td>
        <td class="text-right">13,3</td>
        <td class="text-right">5,6</td>
        <td class="text-right">16,2</td>
        <td class="text-right">16,1</td>
      </tr>
      <tr>
        <td scope="row">Indication de poids (Mt)</td>
        <td class="text-right">2,9</td>
        <td class="text-right">13,1</td>
        <td class="text-right">24,9</td>
        <td class="text-right">12</td>
        <td class="text-right">0,7</td>
      </tr>
      <tr>
        <td scope="row">Taux de collecte (régional)</td>
        <td class="text-right">0,9</td>
        <td class="text-right">9,4</td>
        <td class="text-right">11,7</td>
        <td class="text-right">42,5</td>
        <td class="text-right">8,8</td>
      </tr>
    </tbody>
</table>

Face au flux phénoménale de D.E.E.E. étant produit et non tracé chaque année (44,3 Mt), des pollutions importantes apparaissent dans les pays où sont stockés et enterrés ces déchets. On estime que 50 tonnes de mercure échappent annuellement des e-déchets mis en décharge, ainsi que 71 kt[^105] de plastiques retardateurs de flamme (RFB ou ​BRF​ en anglais)[^106], des substances extrêmement toxiques pour les milieux vivants et sur les personnes qui y sont exposées. Les sites de décharge et de recyclage informel présentent donc des risques sanitaires et environnementaux importants. Les fuites de substances toxiques liées à l’oxydation des composants électroniques polluent les sols et les cours d’eau affectant les animaux, plantations et poissons qui seront consommés par les communautés aux alentours. Les personnes faisant du recyclage informel risquent de respirer des fumées toxiques en brûlant des fils et des circuits imprimés. Ces travailleurs s’exposent malgré eux à des risques plus importants de blessures ainsi qu’à des dommages génétiques, des déséquilibres de glucose dans le sang, des effets sur les fonctions du foi, et des troubles de la fertilité[^107].

Si l’on retourne au niveau de la production des équipements, notamment les phases d’extraction des matières premières, on observe de nombreux risques de pollution des eaux de surface et souterraines autour des zones d’exploitation minière, notamment dû aux écoulements d’acides liés à l’exposition de métaux à l’air libre ou à l’utilisation de cyanure ou d’acide sulfurique pour séparer un élément chimique du minerai. Les différentes fuites, les ruissellements, voire les ruptures de barrage d’une exploitation minière peuvent contaminer les eaux de toute un écosystème.
![Précipitation d’hydroxydes de fer dans un affluent du Missouri recevant des DMA d’une mine de charbon](media/Iron_hydroxide_precipitate_in_stream.jpg "D. Hardesty, USGS Columbia Environmental Research Center")
Les sols et l’air sont aussi pollués par les opérations minières. Les opérations de minage provoquent des émissions de gaz, de poussières et de particules toxiques qui influent sur la qualité de l’air[^108]. Les sols sont contaminés par les poussières soulevées par les opérations de minage ainsi que par le déversement de produits chimiques et de différents résidus dans les sols[^109]. Ces pollutions ont des effets sur l’ensemble des écosystèmes qui y sont exposés, notamment la faune et la flore. La faune aquatique meurt et disparaît dans les cours d’eau contaminés. Ces écoulements toxiques influent aussi sur la santé de la faune et la croissance des plantes qui reposent sur les eaux de surface et souterraines. Enfin, les opérations de minage provoquent des bruits et des vibrations qui affectent la faune locale et la pousse à quitter leur habitat et à fuir.

![Crassier et rejets industriels liés à la production de terres rares au lac Baotou (包头市), Chine](media/unknownfielddivision.jpg "Unknown Fields Division · Liam Young")

{{< aside >}}
#### Quels sont les impacts sanitaires pour les mineurs ?
Les travailleurs des mines sont notamment soumis aux risques suivants :
* éboulement et explosion dans les mines ;
* inhalation des poussières, exposition à des radiations et à des produits chimiques toxiques à l’origine de maladies respiratoires, cancers, affections neurologiques et aberrations chromosomiques ;
* troubles musculaires et squelettiques liés à la pénibilité du travail ;
* maladies liées aux conditions d’hébergement des travailleurs (par exemple cholera)[^110]
{{< /aside >}}<span hidden>[^110]</span>

Les pollutions liées aux exploitations minières et la productions de déchets d’équipements électriques et électroniques continueront à croître tant que l’offre et la demande des équipements numériques seront poussés à la hausse. Face à une consommation toujours croissante de matières, le recyclage des e-déchets est toujours aussi peu favorisé, tant par la conception des équipements que par le niveau d’investissement nécessaire pour un centre de recyclage. Les métaux présents dans les équipements numériques sont difficilement récupérables et recyclables car ils sont présents en trop petite quantité, soit parce qu’ils sont utilisés dans un alliage et ne peuvent pas être séparés. Les industriels du recyclage doivent investir bien plus pour récupérer les métaux par rapport à une autre filière : “Florian Fizaine cite ainsi l’exemple de la société UMICORE : « Un milliard de dollars a été investi dans l’usine de recyclage et de raffinage d’Umicore exploitant des D.E.E.E. en Belgique (Hagelüken et Corti, 2010). Cette usine extrait 30 tonnes d’or, 37 tonnes de métaux du groupe du platine, 1 000 tonnes d’argent et 68 500 tonnes d’autres métaux par an à partir de déchets. Cela en fait la troisième mine d’or du monde […] Par comparaison, une usine de recyclage de papier ne requiert que 30 à 50 millions de dollars d’investissement”[^111]. Finalement le recyclage n’est pas une solution à long-terme et le noeud du problème se situe au niveau l’augmentation de la consommation qui entraîne une hausse de l’extraction, que nous disposions d’une filière de recyclage efficace ou non[^112]. Une croissance constante du couple production/consommation sera inévitablement liée à une augmentation des pollutions dans les eaux, l’air et les sols affectant les écosystèmes et les communautés où se situent les activités d’extraction et de dépôt de déchets.

#### Projections dans l’avenir
Jusqu’à présent nous avons mené un état des lieux du numérique à un moment donné, l’année 2019. Toutefois il est nécessaire de voir quelles sont les perspectives de développement et de consommation du numérique dans les années à venir pour mesurer l’étendue de la réorientation à opérer. Ces projections ne sont pas des prédictions mais, dans le cas présent, elles retentissent comme des alertes car les projections et les taux de croissance des différents impacts environnementaux vont dans le sens contraire aux objectifs de transition.

La demande énergétique de l’infrastructure numérique est croissante et la plupart des projections informent sur une hausse drastique plutôt qu’une stabilisation. Entre 2010 et 2025, GreenIT estime que l’empreinte énergétique du numérique sera multipliée par 2,9 en valeur absolue (x2,4 en valeur relative[^113], c’est-à-dire par rapport à l’évolution de l’empreinte énergétique de l’humanité). En 2019, GreenIT estimait que la consommation d’énergie primaire du numérique représentait 4,2% de la consommation mondiale. Le Shift Project estime pour sa part que le numérique représentait un peu plus de 3,1% de la consommation d’énergie mondiale en 2019 et projette qu’il pourrait 5% de la consommation globale d’ici 2025, avec un taux de croissance de 9% par an si aucune mesure n’est prise[^114]. Le GDS Ecoinfo (CNRS) considère que le numérique (TIC) représente 3,3% de la consommation d’énergie primaire, avec un taux de croissance annuelle de 8%[^115]. Ecoinfo rappelle aussi que le taux de croissance de la consommation énergétique globale est “seulement” de 3%[^116], le numérique est largement au dessus de cette moyenne et double donc sa consommation énergétique tous les 8-9 ans selon son taux de croissance actuel. Finalement, à l’échelle de la France, GreenIT estime que le numérique représentait 6,2% de la consommation d’énergie primaire du pays en 2019[^117].

<table id="tableau7">
    <caption>Tableau comparatif consommation Énergie primaire (en TWh d’énergie primaire)</caption>
    <thead>
      <tr>
        <th class="text-center" scope="col" colspan="4">GreenIT</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="text-center">2010</td>
        <td class="text-center">2019</td>
        <td class="text-center">2025</td>
        <td class="text-center">2019 (Fr)</td>
      </tr>
      <tr>
        <td class="text-center">3400</td>
        <td class="text-center">6800</td>
        <td class="text-center">10000</td>
        <td class="text-center">180</td>
      </tr>
      <tr>
        <td class="text-center">–</td>
        <td class="text-center">4,2%*</td>
        <td class="text-center">–</td>
        <td class="text-center">6,2%**</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="4" style="border:none; padding: 0.75rem 0">* Rapporté à la consommation mondiale annuelle totale <br>** Rapporté à la consommation française annuelle totale</td>
      </tr>
    </tfoot>
</table>

<table id="tableau8">
    <caption>Tableau comparatif consommation finale d’énergie (en TWh)</caption>
    <thead>
      <tr>
        <th scope="col" class="text-center" colspan="3">The Shift Project</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="text-center">2015</td>
        <td class="text-center">2020</td>
        <td class="text-center">2025</td>
      </tr>
      <tr>
        <td class="text-center">2312</td>
        <td class="text-center">2878</td>
        <td class="text-center">4350</td>
      </tr>
      <tr>
        <td class="text-center">–</td>
        <td class="text-center">3,3%*</td>
        <td class="text-center">–</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="3" style="border:none; padding: 0.75rem 0">* Rapporté à la consommation mondiale annuelle totale</td>
      </tr>
    </tfoot>
</table>

Si l’on s’attarde sur la consommation d’électricité liée au numérique GreenIT estime que celle-ci sera multipliée par 2,7 en valeur absolue entre 2010 et 2025 (x1,9 en valeur relative) et qu’elle représentait en 2019 5,5% de la consommation mondiale (1 300 TWh)[^118]. Le Shift Project estimait pour sa part que le numérique représentait 7,6% de la consommation électrique globale en 2017 et la projette à 13,7% d’ici 2025[^119]. À l’échelle française, GreenIT estime que le numérique représentait 8,3% de la consommation électrique totale de la France en 2019 (40 TWh sur 473 TWh)[^120].

<table id="tableau9">
    <caption>Tableau comparatif Électricité (en TWh)</caption>
    <thead>
      <tr>
        <th class="text-center" scope="col" colspan="4">GreenIT</th>
        <th class="text-center" scope="col" colspan="3">The Shift Project</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="text-center">2010</td>
        <td class="text-center">2019</td>
        <td class="text-center">2025</td>
        <td class="text-center">2019 (Fr)</td>
        <td class="text-center">2015</td>
        <td class="text-center">2020</td>
        <td class="text-center">2025</td>
      </tr>
      <tr>
        <td class="text-center">700</td>
        <td class="text-center">1300</td>
        <td class="text-center">1900</td>
        <td class="text-center">40</td>
        <td class="text-center">1646</td>
        <td class="text-center">2179</td>
        <td class="text-center">3840</td>
      </tr>
      <tr>
        <td class="text-center">–</td>
        <td class="text-center">5,5%*</td>
        <td class="text-center">–</td>
        <td class="text-center">8,3%**</td>
        <td class="text-center">–</td>
        <td class="text-center">9,2%*</td>
        <td class="text-center">–</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="7" style="border:none; padding: 0.75rem 0">* Rapporté à la consommation mondiale totale<br>** Rapporté à la consommation française totale</td>
      </tr>
    </tfoot>
</table>

Les émissions de gaz à effet de serre du numérique sont en partie liées à la consommation énergétique donc il paraît cohérent que ces émissions montent aussi. GreenIT estime qu’entre 2010 et 2025 les émissions de GES seront multipliées par 3,1 en valeur absolue (x2,5 en valeur relative)[^121]. Selon le même organisme, les émissions de GES liées au numérique représentaient 3,8% des émissions mondiales en 2019 (1 400 millions de tonnes de gaz à effet de serre). Le Shift Project estime que le numérique représentera 4% des émissions mondiales de GES en 2020 (1 700 millions de tonnes de gaz à effet de serre) avec un taux de croissance annuelle de 8%[^122]. En 2020, le numérique aurait donc émis plus de gaz à effet de serre que l’aviation civile et, s’il suit le taux de croissance actuel, émettra autant que le secteur automobile d’ici 2025. Au niveau de la France, les émissions de GES liées au numérique représentaient 5,2% des émissions nationales (24 millions de tonnes de gaz à effet de serre) d’après GreenIT[^123].

<table id="tableau10">
    <caption>Tableau comparatif GES (en millions de tonnes GES)</caption>
    <thead>
      <tr>
        <th class="text-center" colspan="4" scope="col">GreenIT</th>
        <th class="text-center" colspan="3" scope="col">The Shift Project</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="text-center">2010</td>
        <td class="text-center">2019</td>
        <td class="text-center">2025</td>
        <td class="text-center">2019 (Fr)</td>
        <td class="text-center">2015</td>
        <td class="text-center">2020</td>
        <td class="text-center">2025</td>
      </tr>
      <tr>
        <td class="text-center">738</td>
        <td class="text-center">1400</td>
        <td class="text-center">2278</td>
        <td class="text-center">24</td>
        <td class="text-center">1400</td>
        <td class="text-center">1700</td>
        <td class="text-center">2500</td>
      </tr>
      <tr>
        <td class="text-center">–</td>
        <td class="text-center">3,8%*</td>
        <td class="text-center">–</td>
        <td class="text-center">5,4%**</td>
        <td class="text-center">–</td>
        <td class="text-center">4%*</td>
        <td class="text-center">–</td>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <td colspan="7" style="border:none; padding: 0.75rem 0">* Rapporté aux émissions mondiales annuelles totales<br>** Rapporté aux émissions françaises annuelles totales</td>
      </tr>
    </tfoot>
</table>

Sur la question de l’empreinte en eau, GreenIT indique que la consommation d’eau douce sera multipliée par 2,4 en valeur absolue entre 2010 et 2025 sachant qu’en 2019 le numérique aurait représenté 0,2% de la consommation mondiale (7,8 milliards de m3 d’eau douce dont 1,6 pour la production d’électricité)[^124]. En France, la consommation d’eau du numérique aurait constitué 10,2% de la consommation nationale, soit 559 millions de m3 d’eau douce)[^125].

Concernant les ressources abiotiques, comme les métaux, GreenIT estime que la consommation de ressources abiotiques rares serait de l’ordre de 22 millions de tonnes d’antimoine en 2019 (indice utilisé pour exprimer la contribution à l’épuisement de ressources abiotiques)[^126]. Elle aurait été multipliée par 2,1 en valeur absolue entre 2010 et 2025[^127]. En France, la consommation de ressources abiotiques aurait été de l’ordre de 833 tonnes d’équivalent antimoine en 2019[^128]. L’extraction de ces ressources aurait nécessité l’excavation de 4 milliards de tonnes de terre. Finalement, la production annuelle de e-déchets (D.E.E.E.) était de l’ordre de 53,6 Mt en 2019 et devrait atteindre 74,7 Mt d’ici 2030 d’après le e-Waste Global Monitor[^129]. La quantité de e-déchets générée augmente chaque année de 2 Mt, présumant un taux de croissance annuelle entre 3,5 et 4%, ce qui fait des e-déchets la filière de déchets qui croît le plus.

En valeur absolue, le numérique ne consomme pas autant d’énergie que d’autres secteurs comme le transport ou la bâtiment, toutefois c’est aujourd’hui le secteur qui connaît la plus forte croissance de sa consommation énergétique. Il en va de même avec les émissions de gaz à effet de serre, aucun autre secteur augmente ses émissions à hauteur de 8% par an. Le numérique contribue aussi à la criticité de nombreux métaux et augmente la pression sur le secteur minier, menant à la dégradation accélérée d’écosystèmes et à des pénuries de métaux clés à moyen-terme. La filière des D.E.E.E. ne représentent pas uniquement les déchets liés au numérique toutefois les volumes de déchets augmentent aussi à une vitesse spectaculaire. C’est à cause de tous ces données que le numérique est aujourd’hui scruté. C’est un secteur qui doit intégrer d’urgence les objectifs de transition dans son fonctionnement afin de ne pas compromettre ou ralentir les efforts d’autres secteurs ainsi que les changements individuels et collectifs.

[^20]: Kate Crawford, Vladon Joler, « Anatomy of an AI System », (2018), consulté le 10 août 2020. https://anatomyof.ai/.
[^21]: Our World in Data, « Moore's Law: Transistors per microprocessor », <cite>Our World in Data</cite>, consulté le 10 août 2020. https://ourworldindata.org/grapher/transistors-per-microprocessor.
[^22]: Cécile Diguet, Fanny Lopez, « Territoires numériques et transition énergétique: les limites de la croissance », <cite>Sciences Po</cite> (2019), consulté le 10 août 2020. https://gimelec.fr/wp-content/uploads/2019/06/GIMELEC_DATACENTER_BD.pdf; https://www.sciencespo.fr/ecole-urbaine/sites/sciencespo.fr.ecole-urbaine/files/2019_04%20-%20Diguet%20%26%20Lopez.pdf
[^23]: Cécile Diguet, Fanny Lopez, Laurent Lefèvre, « Impact spatial et énergétique des data centers sur les territoires », <cite>ADEME</cite> (2019), p. 16.
[^24]: France IX, « Réseaux connectés à Paris », consulté le 10 août 2020. https://www.franceix.net/fr/france-ix-paris/members-in-paris/.
[^25]: TeleGeography, « Submarine Cable Frequently Asked Questions », <cite>TeleGeography Blog</cite>, consulté le 10 août 2020. https://www2.telegeography.com/submarine-cable-faqs-frequently-asked-questions.
[^26]: ​Couverture Mobile, « Statistiques », consulté le 10 août 2020. ​https://www.couverture-mobile.fr/?page=statistiques​; ANFR, « Observatoire 2G 3G 4G », consulté le 10 août 2020. https://data.anfr.fr/anfr/visualisation/table/?id=dd11fac6-4531-4a27-9c8c-a3a9e4ec2107​. 
[^27]: Gauthier Roussilhe, “La controverse de la 5G”, 2020, consulté le 10 août 2020. http://gauthierroussilhe.com/fr/projects/controverse-de-la-5g​.
[^28]: ​GreenIT, “Empreinte environnementale du numérique mondial”, 2019, pp. 8-9.
[^29]: ​​Dataportal, “Digital 2020 July Global Statshot Report”, 2020, p. 17. ​https://datareportal.com/global-digital-overview​.
[^30]: ​​Green Alliance, “A circular economy for smart devices Opportunities in the US, UK and India”, 2015, p. 17. https://www.green-alliance.org.uk/resources/A%20circular%20economy%20for%20smart%20devices.pdf​.
[^31]: Fairphone, “Fairphone 2 Supply Chain”, ​Sourcemap​, consulté le 10 août 2020. https://open.sourcemap.com/maps/57bd640851c05c0a5b5a8be1​.
[^32]: ​Kees Baldé et al., “The Global E-waste Monitor 2017”, United Nations University (UNU), International Telecommunication Union (ITU) & International Solid Waste Association (ISWA) (2017), p. 4.
[^33]: Clara Jenik, “Quelle est la durée de vie d’un smartphone ?”, ​Statista​ (2017), consulté le 10 août 2020. https://fr.statista.com/infographie/8334/quelle-est-la-duree-de-vie-dun-smartphone/​.
[^34]: Cisco, “Cisco Visual Networking Index (VNI) Complete Forecast Update, 2017–2022”, 2018.
[^35]: Cisco, “Cisco Visual Networking Index: Global Mobile Data Traffic Forecast Update, 2017–2022”, ​Cisco​ (2019), consulté le 10 août 2020. ​https://s3.amazonaws.com/media.mediapost.com/uploads/CiscoForecast.pdf​.
[^36]: IEA, “Digitalisation and Energy”, 2019, consulté le 10 août 2020. ​https://www.iea.org/reports/digitalisation-and-energy​.
[^37]: Zion Market Research, “Global AI In Oil and Gas Market Will Reach to USD 4.01 Billion By 2025: Zion Market Research”, GlobeNewsWire,​ 18 juillet 2019, consulté le 10 août 2020. https://www.globenewswire.com/news-release/2019/07/18/1884499/0/en/Global-AI-In-Oil-and-Gas-Market-Will-Reach-to-USD-4-01-Billion-By-2025-Zion-Market-Research.html​.
[^38]: ​Brian Merchant, “Amazon Is Aggressively Pursuing Big Oil as It Stalls Out on Clean Energy”, ​Gizmodo,​ 4 août 2019, consulté le 10 août 2020. ​https://gizmodo.com/amazon-is-aggressively-pursuing-big-oil-as-it-stalls-ou-1833875828​.
[^39]: Amazon Web Services, “The Digital Oilfield of the Future”, consulté le 10 août 2020. https://d1.awsstatic.com/Industries/Oil/AWS_O%26G_Overview.pdf​.
[^40]: IEA, “Data Centres and Data Transmission Networks”, Juin 2020, consulté le 10 août 2020. https://www.iea.org/reports/tracking-buildings/data-centres-and-data-transmission-networks​.
[^41]: David Mercer, “Smart Home Will Drive Internet of Things To 50 Billion Devices, Says Strategy Analytics”, Strategy Analytics, 26 octobre 2017, consulté le 10 août 2020. https://www.strategyanalytics.com/strategy-analytics/news/strategy-analytics-press-releases/2017/10/26/smart-home-will-drive-internet-of-things-to-50-billion-devices-says-strategy-analytics​.
[^42]: Dave Evans, “The Internet of ThingsHow the Next Evolution of the Internet: Is Changing Everything”, ​Cisco,​ Avril 2011, consulté le 10 août 2020. ​https://www.cisco.com/c/dam/en_us/about/ac79/docs/innov/IoT_IBSG_0411FINAL.pdf​.
[^43]: ​Mordor Intelligence, “Internet of Things (IoT) Market-Growth, Trends, and Forecast (2020-2025)”, ​Orbis Research,​ 1 février 2020, consulté le 10 août 2020. https://www.orbisresearch.com/reports/index/internet-of-things-iot-market-growth-trends-and-forecast-2020-2025​.
[^44]: Eric Masanet et al., "Recalibrating global data center energy-use estimates", ​Science 3​ 67, n° 6481 (2020), p. 984-986. DOI: 10.1126/science.aba3758; IEA, “Data Centres and Data Transmission Networks”, Juin 2020, consulté le 10 août 2020. https://www.iea.org/fuels-and-technologies/data-centres-networks​.
[^45]: Eric D. Williams, Robert U. Ayres, Miriam Heller, "The 1.7 Kilogram Microchip:  Energy and Material Use in the Production of Semiconductor Devices", ​Environmental Science & Technology​ 36, n° 24 (2002): 5504-5510. DOI: 10.1021/es025643o.
[^46]: ​Sarah B. Boyd, "Life-Cycle Assessment of Semiconductors", (New York: Springer, 2012): 57.
[^47]: ​*Ibid.*, pp. 78-80.
[^48]: ​*Ibid.*, p. 58.
[^49]: Joshua Aslan et al., "Electricity Intensity of Internet Data Transmission: Untangling the Estimates", ​Journal of Industrial Ecology​ 22 (2017), pp. 789-790. DOI:10.1111/jiec.12630. 
[^50]: GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 10.
[^51]: ​GreenIT, "L’électricité n’est pas un indicateur environnemental", ​GreenIT​, 21 janvier 2020, consulté le 11 août 2020. https://www.greenit.fr/2020/01/21/lelectricite-nest-pas-un-indicateur-environnemental/​.
[^52]: Anders S. G. Andrae, Tomas Edler, "On Global Electricity Usage of Communication Technology: Trends to 2030", Challenges​ 6 (2015), pp. 117-157.
[^53]: ​The Shift Project, "LeanICT : pour une sobriété numérique", 2018, pp. 4-5.
[^54]: GreenIT, “Empreinte environnementale du numérique mondial”, 2019.
[^55]: Piers Forster et al., “Changes in Atmospheric Constituents and in Radiative Forcing. In: Climate Change 2007: The Physical Science Basis. Contribution of Working Group I to the Fourth Assessment Report of the Intergovernmental Panel on Climate Change”. (Cambridge, New York: Cambridge University Press, 2007), p. 137.
[^56]: ​GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 10.
[^57]: ​The Shift Project, "LeanICT : pour une sobriété numérique", 2018, pp. 4-5.
[^58]: GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 13.
[^59]: Fridolin Krausmann et al., “Growth in global materials use, GDP and population during the 20th century”, ​Ecological Economics​ 68, n° 10 (2009), pp. 2696-2705. DOI: 10.1016/j.ecolecon.2009.05.007 dans Alain Geldron, “L’épuisement des métaux et minéraux:faut-ils’inquiéter?”,​ADEME,​ 2017, p. 8.
[^60]: 60 ​Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME,​ 2017, p. 8.
[^61]: ​*Ibid.*, p. 6.
[^62]: ​Philippe Bihouix et Benoît de Guillebon, “Quel futur pour les métaux ? Raréfaction des métaux : un nouveau défi pour la société” (Paris : EDP Sciences, 2010), p. 27.
[^63]: Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME,​ 2017, p. 10.
[^64]: ​*Ibid.* p. 11.
[^65]: Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME,​ 2017, p. 12.
[^66]: ​*Ibid.* p. 11.
[^67]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 20.
[^68]: ​Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME,​ 2017, p. 5.
[^69]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 7.
[^70]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 8.
[^71]: *Ibid.*, p. 17.
[^72]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 9.
[^73]: ​*Ibid.*, p. 18.
[^74]: *Ibid.*, p. 9; ARCEP, “Rapport au Parlement sur les coûts de la boucle locale cuivre de France Télécom et leur évolution dans le cadre de la transition du cuivre vers la fibre”, 2011.
[^75]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), pp. 9-10.
[^76]: ​USGC,​ “​Mineral Commodity Summaries”, ​United States Geological Survey​ (USGS) (2019) : le marché mondial était de 1 810 tonnes en 2017 et estimé à 1 800 tonnes en 2018.
[^77]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 10.
[^78]: Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME​, 2017, p. 15.
[^79]: ​*Ibid.*, p. 21; Philippe Bihouix et Benoît de Guillebon, “Quel futur pour les métaux ? Raréfaction des métaux : un nouveau défi pour la société” (Paris : EDP Sciences, 2010), p. 40.
[^80]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 21; Terry Norgate, Sharif Jahanshahi, “Reducing the greenhouse gas footprint of primary metal production: Where should the focus be?”, ​Minerals Engineering​ 24, n°14 (2011), pp. 1563-1570.
[^81]: Florian Fizaine, Victor Court, “Renewable electricity producing technologies and metal depletion: A sensitivity analysis using the EROI”, ​Ecological Economics​ 110 (2015), pp. 106-118. Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME​, 2017, p. 15.
[^82]: Alain Geldron, “L’épuisement des métaux et mineraux : faut-il s’inquiéter ?”, ​ADEME​, 2017, p. 16.
[^83]: Philippe Bihouix et Benoît de Guillebon, “Quel futur pour les métaux ? Raréfaction des métaux : un nouveau défi pour la société” (Paris : EDP Sciences, 2010), p. 29; Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie ​(2020), p. 22.
[^84]: Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME​, 2017, p. 16.
[^85]: ​Camila Montes, "Water use in copper mining:Trends of a critical input", ​Comisión Chilena del Cobre​ (2016) dans Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME​, 2017, p. 15.
[^86]: Christian Piguet, "L’énergie de fabrication est trop souvent négligée", ​Le Temps,​ 27 septembre 2012, consulté le 11 août 2020. [​https://www.letemps.ch/opinions/lenergie-fabrication-souvent-negligee​](https://www.letemps.ch/opinions/lenergie-fabrication-souvent-negligee​).
[^87]: Arjen Y. Hoekstra, “The water footprint of food”, Water Footprint (2008), p. 54, consulté le 11 août 2020. https://waterfootprint.org/media/downloads/Hoekstra-2008-WaterfootprintFood.pdf​.
[^88]: Aiswarya Baskaran, "Waste Not, Want Not – Water Use in the Semiconductor Industry", ​Sustainalytics​, 22 mars 2017, consulté le 11 août 2020. ​[https://www.sustainalytics.com/esg-blog/world-water-day-water-use-semiconductor-industry/​](https://www.sustainalytics.com/esg-blog/world-water-day-water-use-semiconductor-industry/​).
[^89]: Walter Den, Chih-Hao Che, Yung-Chien Luo, "Revisiting the water-use efficiency performance for microelectronics manufacturing facilities: Using Taiwan’s Science Parks as a case study", ​Water-Energy Nexus​ 1, n° 2 (2018), pp. 116-133. DOI:10.1016/j.wen.2018.12.002.
[^90]: Frédéric Bordage, “Sobriété numérique : les clés pour agir”, (Paris : Buchet Chastel, 2019): 7.
[^91]: GreenIT, “Empreinte environnementale du numérique en France”, 2020, p. 8.
[^92]: ​GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 10.
[^93]: Hannah Ritchie, Max Roser, "Water Use and Stress", ​Our World in Data​, 2015, consulté le 11 août 2020. https://ourworldindata.org/water-use-stress#total-freshwater-use​. À titre indicatif, en 2014 les prélèvements d’eau en France (eaux de surface et eaux souterraines) étaient de l’ordre de 5,6 milliards de m3​ ​ d’eau douce : Jean-Louis Pasquier, “Les prélèvements d’eau douce en France : les grands usages en 2013 et leur évolution depuis 20 ans”, ​Datalab​, 2017, p. 9).
[^94]: Cécile Diguet, Fanny Lopez, Laurent Lefèvre, “L’impact spatial et énergétique des data centers sur les territoires”, ​ADEME​, 2019, p. 16.
[^95]: ​GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 17.
[^96]: Perrine Toledano, Clara Roorda, “Levereging mining investments in water infrastructure for broad economic development: models, opportunities and challenges”, ​Columbia Center on Sustainable Investment​ (2015) dans Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie​ (2020), p. 25.
[^97]: ​Alain Geldron, “L’épuisement des métaux et minéraux : faut-il s’inquiéter ?”, ​ADEME​, 2017, p. 15.
[^98]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 26.
[^99]: Vanessa Forti et al. “The Global E-waste Monitor 2020: Quantities, flows and the circular economy potential”, ​United Nations University (UNU)/United Nations Institute for Training and Research (UNITAR) – co-hosted SCYCLE Programme, International Telecommunication Union (ITU) & International Solid Waste Association (ISWA)​, 2020, p. 19.
[^100]: Une Mégatonne est égale à un million de tonnes (1 Mt = 1 000 000 t)
[^101]: Vanessa Forti et al. “The Global E-waste Monitor 2020: Quantities, flows and the circular economy potential”, ​United Nations University (UNU)/United Nations Institute for Training and Research (UNITAR) – co-hosted SCYCLE Programme, International Telecommunication Union (ITU) & International Solid Waste Association (ISWA)​, 2020, p. 14.
[^102]: ​*Ibid.*, p. 24
[^103]: De même, au sein des différentes régions étudiées, le taux de consommation d’équipements et donc la production de déchets ne sont pas les mêmes en fonction des niveaux socio-économiques.
[^104]: ​Vanessa Forti et al. “The Global E-waste Monitor 2020: Quantities, flows and the circular economy potential”, ​United Nations University (UNU)/United Nations Institute for Training and Research (UNITAR) – co-hosted SCYCLE Programme, International Telecommunication Union (ITU) & International Solid Waste Association (ISWA)​, 2020, p. 14.
[^105]: ​1 kt = 1 000 tonnes
[^106]: ​Vanessa Forti et al. “The Global E-waste Monitor 2020: Quantities, flows and the circular economy potential”, ​United Nations University (UNU)/United Nations Institute for Training and Research (UNITAR) – co-hosted SCYCLE Programme, International Telecommunication Union (ITU) & International Solid Waste Association (ISWA)​, 2020, p. 15.
[^107]: ​*Ibid.*, p. 66.
[^108]: Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 27.
[^109]: ​*Ibid.*
[^110]: *Ibid.*
[^111]: ​Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie (2020), p. 41.
[^112]: “L​a logique voudrait que si l’humanité décide de recycler en permanence 75 % des déchets d’un métal donné, elle n’a alors besoin de prélever que 25 % de ses besoins dans les stocks naturels. Or, selon François Grosse, un tel syllogisme ne tient qu’avec une consommation constante dudit métal. En effet, comme l’illustre le graphique 8, en considérant une croissance de 3 % par an de la consommation, le niveau de ponction initial dans les stocks naturels est retrouvé trente ans plus tard (les 25 % de N sont devenus les 100% à N+30).” dans Liliane Dedryver, “La consommation de métaux du numérique : un secteur loin d’être dématérialisé”, France Stratégie​ (2020), p. 43.
[^113]: GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 23.
[^114]: The Shift Project, "LeanICT : pour une sobriété numérique", 2018, pp.16-17.
[^115]: ​Kevin Marquet, Françoise Berthoud, Jacques Combaz, “Introduction aux impacts environnementaux du numérique”, Bulletin de la société informatique de France 13, 2019, p.87.
[^116]: ​*Ibid.*
[^117]: GreenIT, “Empreinte environnementale du numérique en France”, 2020, p. 8.
[^118]: ​GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 23.
[^119]: The Shift Project, "LeanICT : pour une sobriété numérique", 2018, p.63.
[^120]: GreenIT, “Empreinte environnementale du numérique en France”, 2020, p. 8.
[^121]: GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 23.
[^122]: The Shift Project, "LeanICT : pour une sobriété numérique", 2018, pp.17-18.
[^123]: GreenIT, “Empreinte environnementale du numérique en France”, 2020, p. 8.
[^124]: ​GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 23.
[^125]: GreenIT, “Empreinte environnementale du numérique en France”, 2020, p. 8.
[^126]: GreenIT, “Empreinte environnementale du numérique mondial”, 2019, p. 10.
[^127]: *Ibid.*, p. 23.
[^128]: GreenIT, “Empreinte environnementale du numérique en France”, 2020, p. 8.
[^129]:​ Vanessa Forti et al. “The Global E-waste Monitor 2020: Quantities, flows and the circular economy potential”, ​United Nations University (UNU)/United Nations Institute for Training and Research (UNITAR) – co-hosted SCYCLE Programme, International Telecommunication Union (ITU) & International Solid Waste Association (ISWA)​, 2020, p. 13.