[![Netlify Status](https://api.netlify.com/api/v1/badges/7530edea-57b3-402e-809e-04b8ef1eac68/deploy-status)](https://app.netlify.com/sites/situer-le-numerique/deploys)

# Situer le numérique

Projet de mise en page à l’aide de la bibliothèque [Paged.js](https://www.pagedjs.org/) de l’ouvrage <cite>Situer le numérique</cite> rédigé par [Gauthier Roussihle](http://gauthierroussilhe.com/fr). 
La documentation de ce projet (consultable [ici](http://pad.designcommun.fr/s/BJWk-A3QD)) s’inscrit dans le cadre de la thèse en ergonomie et design graphique au laboratoire Paragraphe (Université Paris 8) et à EnsadLab-Paris de [Julie Blanc](https://julie-blanc.fr/).

## Installation

Si [Hugo](https://gohugo.io/getting-started/installing/) n’est pas installé sur votre machine, installer le avec la commande :

```shell
brew install hugo
```

## Usage

Clonez ou téléchargez ce dossier, ouvrez le dans votre Terminal et tapez la commande suivante pour lancer un serveur local :

```shell
hugo server
```

Pour générer un PDF, ouvrez l’adresse du serveur local dans Chromium, attendez une dizaine de secondes que tout les contenus et les scripts se chargent, puis lancez l’impression de la page et sélectionnez l’option de Destination `Enregistrer en tant que PDF`
